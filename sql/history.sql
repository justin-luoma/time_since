create table history
(
    id         bigint primary key generated always as identity               not null,
    times_id   bigint references times(id) on delete cascade                not null,
    created_by uuid references auth.users                                    not null,
    created_at timestamp with time zone default timezone('utc'::text, now()) not null,
    history    json
);


alter table history
    enable row level security;

create
    policy "Allow individual read access" on history
    for
    select using (auth.uid() = created_by);

create
    policy "Allow individual insert access" on history
    for
    insert with check (auth.uid() = created_by);

create
    policy "Allow individual update access" on history
    for
    update using (auth.uid() = created_by);
