create table reports
(
    id          bigint primary key generated always as identity               not null,
    created_by  uuid references auth.users                                    not null,
    created_at  timestamp with time zone default timezone('utc'::text, now()) not null,
    report text
);

alter table reports
    enable row level security;

create
    policy "Allow individual read access" on reports
    for
    select using (auth.uid() = created_by);

create
    policy "Allow individual insert access" on reports
    for
    insert with check (auth.uid() = created_by);

create
    policy "Allow individual update access" on reports
    for
    update using (auth.uid() = created_by);

create
    policy "Allow individual delete access" on reports
    for
    delete
    using (auth.uid() = created_by);
