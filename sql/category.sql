create table category
(
    id         bigint primary key generated always as identity               not null,
    name       text                                                          not null,
    color      text                     default '#ff424242'                  not null,
    created_by uuid references auth.users                                    not null,
    created_at timestamp with time zone default timezone('utc'::text, now()) not null
);

alter table category
    enable row level security;

create
    policy "Allow individual read access" on category
    for
    select using (auth.uid() = created_by);

create
    policy "Allow individual insert access" on category
    for
    insert with check (auth.uid() = created_by);

create
    policy "Allow individual update access" on category
    for
    update using (auth.uid() = created_by);

create
    policy "Allow individual delete access" on category
    for
    delete
    using (auth.uid() = created_by);

-- create table times_category
-- (
--     times_id    bigint references times (id)    not null,
--     category_id bigint references category (id) not null
-- );

alter table times
    add column category bigint references category (id) on delete set null null;

alter table category add column color text default '#ff424242' not null;
