create table times
(
    id          bigint primary key generated always as identity               not null,
    created_by  uuid references auth.users                                    not null,
    created_at  timestamp with time zone default timezone('utc'::text, now()) not null,
    name        text                                                          not null,
    timestamp   timestamp with time zone default timezone('utc'::text, now()) not null,
    description text,
    image_url   text,
    frequency   bigint
);

alter table times
    enable row level security;

create
policy "Allow individual read access" on times
  for
select using (auth.uid() = created_by);

create
policy "Allow individual insert access" on times
  for
insert with check (auth.uid() = created_by);

create
policy "Allow individual update access" on times
  for
update using (auth.uid() = created_by);

create
policy "Allow individual delete access" on times
  for
delete
using (auth.uid() = created_by);

alter
publication supabase_realtime
  add table times;

create
or replace function getTimesSortByDueDate(created_by_id uuid) returns table(id bigint, created_by uuid, name
text, due_date timestamp)
    language sql
    as $$
select *
from (select id,
             created_by,
             name,
          timestamp + interval '1'
          day *
          frequency as
          due_date
      from times) query
where created_by = created_by_id;
$$;

insert into storage.buckets (id, name)
values ('images', 'images');

create
policy "Images are private" on storage.objects
    for
select using (
    bucket_id = 'images'
    and auth.uid() = owner);

create
policy "Owner can upload" on storage.objects
    for
insert with check (
    bucket_id = 'images'
    and auth.uid() = owner);

create
policy "Owner can update" on storage.objects
    for
update with check (
    bucket_id = 'images'
    and auth.uid() = owner);

create
policy "Owner can delete" on storage.objects
    for
delete
using (
    bucket_id = 'images'
    and auth.uid() = owner);
