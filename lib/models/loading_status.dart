class LoadingStatus {
  Status status;
  String? message;

  LoadingStatus.initial(this.message) : status = Status.initial;

  LoadingStatus.loading(this.message) : status = Status.loading;

  LoadingStatus.completed() : status = Status.completed;

  LoadingStatus.error(this.message) : status = Status.error;
}

enum Status { initial, loading, completed, error }
