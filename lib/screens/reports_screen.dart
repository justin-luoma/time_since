import 'package:flutter/material.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:time_since/components/auth/auth_required_state.dart';
import 'package:time_since/utils/constants.dart';
import 'package:time_since/utils/validators.dart';

class ReportsScreen extends StatefulWidget {
  @override
  _ReportsScreenState createState() => _ReportsScreenState();
}

class _ReportsScreenState extends AuthRequiredState<ReportsScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  late final TextEditingController _reportController;
  late final SupabaseClient _supabase;

  bool _loading = false;

  @override
  void initState() {
    super.initState();
    _reportController = TextEditingController();
    _supabase = supabase;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Create Report'),
        actions: [
          IconButton(
            onPressed: _loading ? null : () async {
              await _submitReport();
            },
            icon: _loading
                ? const Icon(Icons.pending)
                : const Icon(Icons.done),
          ),
        ],
      ),
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Form(
                key: _formKey,
                child: TextFormField(
                  controller: _reportController,
                  decoration: const InputDecoration(
                    hintText: 'Report',
                  ),
                  textCapitalization: TextCapitalization.sentences,
                  scrollPadding: const EdgeInsets.all(20.0),
                  keyboardType: TextInputType.multiline,
                  maxLines: 99999,
                  autofocus: true,
                  validator: Validators.requiredValidator,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Future<void> _submitReport() async {
    if (_formKey.currentState!.validate()) {
      final user = _supabase.auth.user();
      if (user != null) {
        final res = await _supabase.from('reports').insert(
            {
              'report': _reportController.text,
              'created_by': user.id
            }
        ).execute();
        if (res.hasError) {
          debugPrint(res.error.toString());
          context.showErrorSnackBar(message: 'Error please try again');
        }
        Navigator.of(context).pop();
      }
    }
  }
}
