import 'package:time_since/supabase_config.dart';

class Config {
  static String supabaseURL = SupabaseConfig.supabaseURL;
  static String supabaseAnonPublicKey = SupabaseConfig.supabaseAnonPublicKey;

  static const String UPCOMING_TIME_CHANNEL_ID = "UPCOMING_TIME";
  static const String UPCOMING_TIME_GROUP_ID = "UPCOMING_TIME_GROUP";
  static const String UPCOMING_TIME_GROUP_NAME = "Upcoming Items(s) Group";
  static const String UPCOMING_TIME_CHANNEL_NAME = "Upcoming Item(s)";
  static const UPCOMING_TIME_GROUP_KEY = 'dev.luoma'
      '.time_since.UPCOMING_TIME_GROUP';

  static const String lastNotificationKey = 'LAST_NOTIFICATION';
  static const String emailKey = "LOGIN_EMAIL";
  static const String passKey = "LOGIN_PASS";
  static const String cryptoKey = "CRYPTO";
  static const String ivKey = 'IV';
}
