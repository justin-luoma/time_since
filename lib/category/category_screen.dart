import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:time_since/category/components/new_category_dialog.dart';
import 'package:time_since/category/models/categorized_times_model.dart';
import 'package:time_since/category/pages/update_category_page.dart';
import 'package:time_since/category/view_models/category_view_model.dart';
import 'package:time_since/colors/extensions/color.dart';
import 'package:time_since/colors/view_models/color_picker_view_model.dart';
import 'package:time_since/components/auth/auth_required_state.dart';
import 'package:time_since/components/confirm_dialog.dart';
import 'package:time_since/models/loading_status.dart';
import 'package:time_since/times/components/time_card.dart';
import 'package:time_since/times/models/time_model.dart';
import 'package:time_since/times/view_models/times_view_model.dart';
import 'package:time_since/utils/time.dart';

class CategoryScreen extends StatefulWidget {
  @override
  _CategoryScreenState createState() => _CategoryScreenState();
}

class _CategoryScreenState extends AuthRequiredState<CategoryScreen> {
  final Map<int, bool> _folderOpen = Map.identity();
  final Map<int, bool> _timeOpen = Map.identity();

  @override
  void initState() {
    super.initState();
    Future.microtask(() async {
      final categoryViewModel =
          Provider.of<CategoryViewModel>(context, listen: false);
      categoryViewModel.refreshCategories();
    });
  }

  @override
  Widget build(BuildContext context) {
    final categoryViewModel = Provider.of<CategoryViewModel>(context);
    final style =
        DefaultTextStyle.of(context).style.merge(const TextStyle(fontSize: 20));
    final List<CategorizedTimesModel> categorizedTimes =
        categoryViewModel.categorizedTimes;

    Widget _collapsedContent(
        Iterable<TimeModel> times, CategorizedTimesModel category) {
      return Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          if (times.isNotEmpty) _buildTimesListView(times),
          Padding(
            padding: const EdgeInsets.only(top: 8.0, left: 8.0, right: 8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                ElevatedButton(
                  onPressed: () {
                    showDialog(
                        context: context,
                        builder: (context) => ConfirmDialog(
                            title: 'Deleting ${category.name}',
                            onConfirm: () => _deleteCategory(category)));
                  },
                  style: ElevatedButton.styleFrom(
                    primary: Colors.pink,
                  ),
                  child: const Text('Delete'),
                ),
                ElevatedButton(
                  onPressed: () {
                    categoryViewModel.categoryBeingUpdated = category;
                    Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => MultiProvider(
                        providers: [
                          ChangeNotifierProvider.value(
                              value: categoryViewModel),
                          ChangeNotifierProvider(
                            create: (context) => ColorPickerViewModel(
                              HexColor.fromHex(category.color),
                            ),
                          )
                        ],
                        child: const UpdateCategoryPage(),
                      ),
                    ));
                  },
                  // style: ElevatedButton.styleFrom(
                  //   primary: Colors.pink,
                  // ),
                  child: const Text('Edit'),
                ),
              ],
            ),
          ),
        ],
      );
    }

    Widget _buildCategoryCard(CategorizedTimesModel category) {
      final times = category.times;
      String? nextDue = _nextDueText(times);

      final backgroundColor = HexColor.fromHex(category.color);

      final itemsDue = _itemsDue(times) > 0;
      return GestureDetector(
        onTap: () {
          setState(() {
            _folderOpen.update(
              category.id,
              (value) => !value,
              ifAbsent: () => true,
            );
          });
        },
        child: Card(
          color: itemsDue
              ? const Color.fromRGBO(181, 63, 81, 100.0)
              : Theme.of(context).appBarTheme.foregroundColor,
          child: Column(
            children: [
              Container(
                padding:
                    const EdgeInsets.symmetric(vertical: 8.0, horizontal: 18),
                decoration: BoxDecoration(
                    color: itemsDue ? null : backgroundColor,
                    borderRadius: const BorderRadius.all(Radius.circular(4))),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Flexible(
                          fit: FlexFit.tight,
                          child: FittedBox(
                            alignment: Alignment.centerLeft,
                            fit: BoxFit.scaleDown,
                            child: Text(
                              category.name,
                              style: const TextStyle(fontSize: 30),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        if (times.isNotEmpty)
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Flexible(
                                fit: FlexFit.tight,
                                child: FittedBox(
                                  alignment: Alignment.centerLeft,
                                  fit: BoxFit.scaleDown,
                                  child: Text(
                                    _dueText(times),
                                    style: const TextStyle(fontSize: 20),
                                  ),
                                ),
                              )
                            ],
                          ),
                        if (nextDue != null)
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Flexible(
                                fit: FlexFit.tight,
                                child: FittedBox(
                                  alignment: Alignment.centerLeft,
                                  fit: BoxFit.scaleDown,
                                  child: Text(
                                    nextDue,
                                    style: const TextStyle(fontSize: 20),
                                  ),
                                ),
                              )
                            ],
                          ),
                      ],
                    ),
                  ],
                ),
              ),
              _folderOpen.containsKey(category.id) && _folderOpen[category.id]!
                  ? _collapsedContent(times, category)
                  : const SizedBox(
                      height: 0,
                    ),
            ],
          ),
        ),
      );
    }

    Row _addCategoryRow(BuildContext context, TextStyle style) {
      final defaultColor =
          Theme.of(context).cardTheme.color ?? Theme.of(context).cardColor;
      return Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextButton.icon(
              onPressed: () async {
                await showDialog(
                    context: context,
                    builder: (context) => MultiProvider(
                          providers: [
                            ChangeNotifierProvider(
                              create: (context) =>
                                  ColorPickerViewModel(defaultColor),
                            ),
                            ChangeNotifierProvider.value(
                                value: categoryViewModel)
                          ],
                          child: const NewCategoryDialog(),
                        ));
              },
              icon: Icon(
                Icons.add,
                color: style.color!,
              ),
              label: Text(
                'Add Category',
                style: style,
              ),
            ),
          ),
        ],
      );
    }

    Widget _mainContent(
        List<CategorizedTimesModel> categorizedTimes, TextStyle style) {
      return Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          categorizedTimes.isNotEmpty
              ? Expanded(
                  child: ListView.builder(
                    itemCount: categorizedTimes.length + 1,
                    padding: const EdgeInsets.symmetric(
                      vertical: 18,
                      horizontal: 12,
                    ),
                    shrinkWrap: true,
                    itemBuilder: (context, index) {
                      return index < categorizedTimes.length
                          ? _buildCategoryCard(
                              categorizedTimes.elementAt(index))
                          : _addCategoryRow(context, style);
                    },
                  ),
                )
              : Expanded(
                  child: ListView(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: const [
                          Padding(
                            padding: EdgeInsets.only(top: 18.0),
                            child: Text('No Categories'),
                          )
                        ],
                      ),
                      _addCategoryRow(context, style)
                    ],
                  ),
                ),
        ],
      );
    }

    return RefreshIndicator(
      onRefresh: () async {
        await categoryViewModel.getCategorizedTimes();
      },
      child: categoryViewModel.loadingStatus.status == Status.loading
          ? const Center(
              child: CircularProgressIndicator(),
            )
          : categoryViewModel.loadingStatus.status == Status.error
              ? ListView(
                  children: [
                    Center(
                      child: Text(categoryViewModel.loadingStatus.message!),
                    ),
                  ],
                )
              : categoryViewModel.loadingStatus.status == Status.initial
                  ? Text(categoryViewModel.loadingStatus.message!)
                  : _mainContent(categorizedTimes, style),
    );
  }

  ListView _buildTimesListView(Iterable<TimeModel> times) {
    return ListView.builder(
      padding: const EdgeInsets.symmetric(
        vertical: 24,
        horizontal: 18,
      ),
      itemCount: times.length,
      shrinkWrap: true,
      physics: const ClampingScrollPhysics(),
      itemBuilder: ((context, index) {
        final time = times.elementAt(index);
        final open = _timeOpen.containsKey(time.id) && _timeOpen[time.id]!;
        return TimeCard(time, open, (open) {
          setState(() {
            _timeOpen.update(
              time.id,
              (value) => open,
              ifAbsent: () => open,
            );
          });
        });
      }),
    );
  }

  _deleteCategory(CategorizedTimesModel category) async {
    await Provider.of<CategoryViewModel>(context, listen: false)
        .deleteCategory(category.id);
    await Provider.of<TimesViewModel>(context, listen: false).getTimes();
  }
}

String _dueText(Iterable<TimeModel> times) {
  int itemsDue = _itemsDue(times);

  return itemsDue == 0
      ? 'None need attention'
      : '$itemsDue item${itemsDue == 1 ? '' : ''
              's'} '
          'need attention';
}

int _itemsDue(Iterable<TimeModel> times) {
  int itemsDue = 0;
  for (final time in times) {
    if (isDue(time)) {
      itemsDue++;
    }
  }
  return itemsDue;
}

String? _nextDueText(Iterable<TimeModel> times) {
  final timesWithDueDates =
      times.where((time) => time.dueDate != null).toList();
  timesWithDueDates.sort(
    (a, b) => a.dueDate!.compareTo(b.dueDate!),
  );

  if (timesWithDueDates.isNotEmpty) {
    return 'Upcoming: ${timesWithDueDates[0].name}';
  }
  return null;
}
