class CategoryModel {
  final int id;
  final String name;
  final String color;
  final String createdBy;
  final DateTime createdAt;

  CategoryModel(this.id, this.name, this.color, this.createdBy, this.createdAt);

  factory CategoryModel.fromJson(Map<String, dynamic> json) {
    return CategoryModel(
      json['id'],
      json['name'],
      json['color'],
      json['created_by'],
      DateTime.parse(json['created_at']),
    );
  }

  @override
  String toString() {
    return 'CategoryModel{id: $id, name: $name, color: $color, createdBy: $createdBy, createdAt: $createdAt}';
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is CategoryModel &&
          runtimeType == other.runtimeType &&
          id == other.id;

  @override
  int get hashCode => id.hashCode;
}
