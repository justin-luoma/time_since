import 'package:time_since/times/models/time_model.dart';
import 'package:time_since/utils/time.dart';

class CategorizedTimesModel {
  final int id;
  final String name;
  final String color;
  final String createdBy;
  final DateTime createdAt;
  final List<TimeModel> times;

  CategorizedTimesModel(this.id, this.name, this.color, this.createdBy,
      this.createdAt, this.times);

  factory CategorizedTimesModel.fromJson(Map<String, dynamic> json) {
    final times = (json['times'] as List<dynamic>)
        .map((e) => TimeModel.fromJson(e))
        .toList();
    times.sort(dueDateSorter);
    return CategorizedTimesModel(
      json['id'],
      json['name'],
      json['color'],
      json['created_by'],
      DateTime.parse(json['created_at']),
      times,
    );
  }

  @override
  String toString() {
    return 'CategorizedTimesModel{id: $id, name: $name, color: $color, createdBy: $createdBy, createdAt: $createdAt, times: $times}';
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is CategorizedTimesModel &&
          runtimeType == other.runtimeType &&
          id == other.id;

  @override
  int get hashCode => id.hashCode;

// factory CategorizedTimesModel.fromJson()

}
