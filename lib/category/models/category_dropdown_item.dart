class CategoryDropdownItem {
  CategoryDropdownItem(this.id, this.name);

  final int id;
  final String name;

  @override
  String toString() {
    return 'CategoryDropdownItem{id: $id, name: $name}';
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is CategoryDropdownItem &&
          runtimeType == other.runtimeType &&
          id == other.id;

  @override
  int get hashCode => id.hashCode;
}
