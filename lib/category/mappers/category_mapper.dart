import 'package:time_since/category/models/categorized_times_model.dart';
import 'package:time_since/category/models/category_model.dart';

class CategoryMapper {
  static CategorizedTimesModel categoryToCategorizedTimes(
      CategoryModel category) {
    return CategorizedTimesModel(
      category.id,
      category.name,
      category.color,
      category.createdBy,
      category.createdAt,
      List.empty(),
    );
  }
}
