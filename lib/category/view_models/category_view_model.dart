import 'package:flutter/foundation.dart';
import 'package:time_since/cache/service/cache_service.dart';
import 'package:time_since/category/mappers/category_mapper.dart';
import 'package:time_since/category/models/categorized_times_model.dart';
import 'package:time_since/category/models/category_model.dart';
import 'package:time_since/category/service/category_service.dart';
import 'package:time_since/models/loading_status.dart';
import 'package:time_since/utils/time.dart';
import 'package:time_since/view_models/base_view_model.dart';

class CategoryViewModel extends ChangeNotifier with BaseViewModel {
  CategoryViewModel(this.categoryService, this.cacheService);

  final CategoryService categoryService;
  final CacheService cacheService;

  LoadingStatus loadingStatus = LoadingStatus.initial('No folders');

  List<CategoryModel> categories = List.empty(growable: true);
  List<CategorizedTimesModel> categorizedTimes = List.empty(growable: true);
  DateTime _lastUpdate = DateTime(1990);

  CategorizedTimesModel? categoryBeingUpdated;

  Future<void> getCategories() async {
    loadingStatus = LoadingStatus.loading('Fetching Categories');
    notifyListeners();
    try {
      categories = await categoryService.getCategories();
      loadingStatus = LoadingStatus.completed();
      notifyListeners();
    } catch (err) {
      debugPrint(err.toString());
      loadingStatus = LoadingStatus.error('Failed to get categories, try again '
          'later');
      notifyListeners();
    }
  }

  Future<int?> createCategory(String name, String color) async {
    debugPrint(color);
    loadingStatus = LoadingStatus.loading('Creating');
    notifyListeners();
    try {
      final category = await categoryService.createCategory(name, color);
      if (categories.isNotEmpty) {
        categories.add(category);
      }
      categorizedTimes.add(CategoryMapper.categoryToCategorizedTimes(category));
      loadingStatus = LoadingStatus.completed();
      notifyListeners();
      return category.id;
    } catch (err) {
      debugPrint('createCategory error: ${err.toString()}');
      loadingStatus =
          LoadingStatus.error("Failed to save, please refresh and try again");
      notifyListeners();
    }
    return null;
  }

  Future<void> getCategorizedTimes() async {
    loadingStatus = LoadingStatus.loading('Fetching categories');
    notifyListeners();
    try {
      categorizedTimes = await categoryService.getCategorizedTimes();
      categorizedTimes.sort(categoryDueDateSorter);
      await _updateImages();
      _lastUpdate = DateTime.now();
      loadingStatus = LoadingStatus.completed();
      notifyListeners();
    } catch (err) {
      debugPrint(err.toString());
      loadingStatus = LoadingStatus.error('Failed to get categories, try again '
          'later');
      notifyListeners();
    }
  }

  Future<void> deleteCategory(int id) async {
    loadingStatus = LoadingStatus.loading('Deleting');
    notifyListeners();
    try {
      await categoryService.deleteCategory(id);
      if (categories.isNotEmpty) {
        categories.removeWhere((category) => category.id == id);
      }
      categorizedTimes.removeWhere((category) => category.id == id);
      loadingStatus = LoadingStatus.completed();
      notifyListeners();
    } catch (err) {
      debugPrint('deleteCategory error: ${err.toString()}');
      loadingStatus =
          LoadingStatus.error("Failed to delete, please refresh and try again");
      notifyListeners();
    }
  }

  Future<void> updateCategory(int id, String name, String color) async {
    loadingStatus = LoadingStatus.loading('Saving...');
    notifyListeners();
    try {
      final update = {
        'name': name,
        'color': color,
      };
      await categoryService.updateCategory(id, update);
      categorizedTimes.removeWhere((element) => element.id == id);
      final updatedCategorizedTime = CategorizedTimesModel(
          id,
          name,
          color,
          categoryBeingUpdated!.createdBy,
          categoryBeingUpdated!.createdAt,
          categoryBeingUpdated!.times);
      categorizedTimes.add(updatedCategorizedTime);
      categorizedTimes.sort(categoryDueDateSorter);
      loadingStatus = LoadingStatus.completed();
      notifyListeners();
    } catch (err) {
      debugPrint(err.toString());
      loadingStatus = LoadingStatus.error('Failed to save, try again '
          'later');
      notifyListeners();
    }
  }

  Future<void> refreshCategories() async {
    await refresh(_lastUpdate, getCategorizedTimes);
  }

  Future<void> _updateImages() async {
    for (var category in categorizedTimes) {
      for (var time in category.times) {
        if (time.imageUrl != null) {
          time.image = await cacheService.getImage(time.imageUrl!);
        }
      }
    }
  }
}
