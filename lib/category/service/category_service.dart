import 'package:postgrest/src/postgrest_response.dart';
import 'package:time_since/category/models/categorized_times_model.dart';
import 'package:time_since/category/models/category_model.dart';
import 'package:time_since/service/supabase_service.dart';

class CategoryService extends SupabaseService {
  CategoryService(super.client);

  Future<List<CategoryModel>> getCategories() async {
    final user = await getUser();
    if (user != null) {
      final res = await client
          .from('category')
          .select()
          .eq('created_by', user.id)
          .execute();
      if (res.hasError) {
        final errorMessage = res.error.toString();
        throw Exception(errorMessage);
      } else if (res.data != null) {
        return _parseCategoryData(res.data);
      }
      return List.empty();
    } else {
      throw Exception('No user');
    }
  }

  Future<List<CategorizedTimesModel>> getCategorizedTimes() async {
    final user = await getUser();
    if (user != null) {
      final res = await client
          .from('category')
          .select('''
            *,
            times (*, category(*))
          ''')
          .eq('created_by', user.id)
          .order('timestamp', foreignTable: 'times', ascending: true)
          .execute();
      if (res.hasError) {
        final errorMessage = res.error.toString();
        throw Exception(errorMessage);
      } else if (res.data != null) {
        return _parseCategorizedTimesData(res.data);
      }
      return List.empty();
    } else {
      throw Exception('No user');
    }
  }

  Future<CategoryModel> updateCategory(
      int id, Map<String, dynamic> update) async {
    final user = await getUser();
    if (user != null) {
      final res = await client
          .from('category')
          .update(update)
          .match({'id': id}).execute();
      if (res.hasError) {
        final errorMessage = res.error.toString();
        throw Exception(errorMessage);
      } else if (res.data != null) {
        return _parseCategoryData(res.data)[0];
      } else {
        throw Exception('No data returned');
      }
    } else {
      throw Exception('No user');
    }
  }

  Future<CategoryModel?> getCategory(int id) async {
    final user = await getUser();
    if (user != null) {
      final PostgrestResponse res =
          await client.from('category').select().eq('id', id).execute();
      if (res.hasError) {
        final errorMessage = res.error.toString();
        throw Exception(errorMessage);
      } else if (res.data != null &&
          res.data.runtimeType == List &&
          (res.data as List).isNotEmpty) {
        return CategoryModel.fromJson(res.data[0]);
      }
    } else {
      throw Exception('No user');
    }
    return null;
  }

  Future<CategoryModel> createCategory(String name, String color) async {
    final user = await getUser();
    if (user != null) {
      final res = await client.from('category').insert({
        'name': name,
        'color': color,
        'created_by': user.id,
      }).execute();
      if (res.hasError) {
        final errorMessage = res.error.toString();
        throw Exception(errorMessage);
      } else {
        return CategoryModel.fromJson(res.data[0]);
      }
    } else {
      throw Exception('No user');
    }
  }

  Future<void> deleteCategory(int id) async {
    final user = await getUser();
    if (user != null) {
      final res =
          await client.from('category').delete().match({'id': id}).execute();
      if (res.hasError) {
        final errorMessage = res.error.toString();
        throw Exception(errorMessage);
      }
    } else {
      throw Exception('No user');
    }
  }

  List<CategoryModel> _parseCategoryData(List<dynamic> data) {
    return data.map((e) => CategoryModel.fromJson(e)).toList();
  }

  List<CategorizedTimesModel> _parseCategorizedTimesData(List<dynamic> data) {
    return data.map((e) => CategorizedTimesModel.fromJson(e)).toList();
  }
}
