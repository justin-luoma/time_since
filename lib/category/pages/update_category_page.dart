import 'package:flutter/material.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:provider/provider.dart';
import 'package:time_since/category/view_models/category_view_model.dart';
import 'package:time_since/colors/components/color_picker_dialog.dart';
import 'package:time_since/colors/extensions/color.dart';
import 'package:time_since/colors/view_models/color_picker_view_model.dart';
import 'package:time_since/components/auth/auth_required_state.dart';
import 'package:time_since/utils/constants.dart';
import 'package:time_since/utils/validators.dart';

class UpdateCategoryPage extends StatefulWidget {
  const UpdateCategoryPage({Key? key}) : super(key: key);

  @override
  State<UpdateCategoryPage> createState() => _UpdateCategoryPageState();
}

class _UpdateCategoryPageState extends AuthRequiredState<UpdateCategoryPage> {
  final GlobalKey<FormState> categoryCreateFrom = GlobalKey<FormState>();
  late final TextEditingController nameController;

  bool _loading = false;

  @override
  void initState() {
    super.initState();
    nameController = TextEditingController();
    Future.microtask(() {
      final vm = Provider.of<CategoryViewModel>(context, listen: false);
      nameController.text = vm.categoryBeingUpdated!.name;
    });
  }

  @override
  void dispose() {
    nameController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final vm = Provider.of<CategoryViewModel>(context);
    final colorPickerViewModel = Provider.of<ColorPickerViewModel>(context);

    return Scaffold(
      appBar: AppBar(
        title: Text('Updating ${vm.categoryBeingUpdated!.name}'),
        actions: <Widget>[
          IconButton(
              icon: _loading
                  ? const Icon(Icons.pending)
                  : const Icon(Icons.check),
              onPressed: _loading ? null : () async {
                if (categoryCreateFrom.currentState!.validate()) {
                  setState(() {
                    _loading = true;
                  });
                  await vm.updateCategory(vm.categoryBeingUpdated!.id,
                      nameController.text, colorPickerViewModel.color.toHex());
                  setState(() {
                    _loading = false;
                  });
                  if (mounted) {
                    Navigator.of(context).pop();
                  }
                } else {
                  context.showErrorSnackBar(message: 'Fix the form');
                }
              }),
        ],
      ),
      body: SafeArea(
        child: ListView(
          padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 12),
          children: [
            Form(
              key: categoryCreateFrom,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  TextFormField(
                    controller: nameController,
                    validator: Validators.requiredValidator,
                    textCapitalization: TextCapitalization.words,
                    decoration: const InputDecoration(labelText: 'Name'),
                  ),
                  Row(
                    children: [
                      const Text('Color: '),
                      Padding(
                        padding: const EdgeInsets.only(top: 8.0),
                        child: ColorIndicator(
                          HSVColor.fromColor(colorPickerViewModel.color),
                        ),
                      )
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 8.0),
                        child: ElevatedButton(
                          onPressed: () {
                            showDialog(
                              context: context,
                              builder: (context) =>
                                  ChangeNotifierProvider.value(
                                value: colorPickerViewModel,
                                child: const ColorPickerDialog(),
                              ),
                            );
                          },
                          child: const Text('Pick a color '
                              '(optional)'),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
