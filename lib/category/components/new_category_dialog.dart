import 'package:flutter/material.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:provider/provider.dart';
import 'package:time_since/category/view_models/category_view_model.dart';
import 'package:time_since/colors/view_models/color_picker_view_model.dart';
import 'package:time_since/colors/components/color_picker_dialog.dart';
import 'package:time_since/colors/extensions/color.dart';
import 'package:time_since/components/auth/auth_required_state.dart';
import 'package:time_since/utils/constants.dart';
import 'package:time_since/utils/validators.dart';

class NewCategoryDialog extends StatefulWidget {
  const NewCategoryDialog({Key? key}) : super(key: key);

  @override
  State<NewCategoryDialog> createState() => _NewCategoryDialogState();
}

class _NewCategoryDialogState extends AuthRequiredState<NewCategoryDialog> {
  final GlobalKey<FormState> categoryCreateFrom = GlobalKey<FormState>();
  final nameController = TextEditingController();

  bool _loading = false;

  @override
  void initState() {
    super.initState();
  }


  @override
  void dispose() {
    nameController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final categoryViewModel =
        Provider.of<CategoryViewModel>(context, listen: false);
    final colorPickerViewModel = Provider.of<ColorPickerViewModel>(context);

    return AlertDialog(
      title: const Text('New Category'),
      content: Form(
        key: categoryCreateFrom,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            TextFormField(
              textCapitalization: TextCapitalization.words,
              controller: nameController,
              validator: Validators.requiredValidator,
              decoration: const InputDecoration(labelText: 'Name'),
            ),
            Row(
              children: [
                const Text('Color: '),
                Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: ColorIndicator(HSVColor.fromColor(colorPickerViewModel.color)),
                )
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: ElevatedButton(
                      onPressed: () {
                        showDialog(
                            context: context,
                            builder: (context) => ChangeNotifierProvider.value(value: colorPickerViewModel,
                                child:
                                const
                            ColorPickerDialog()));
                      },
                      child: const Text('Pick a color '
                          '(optional)'),
                    ),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
      actions: [
        TextButton(
          onPressed: _loading ? null : () async {
            if (categoryCreateFrom.currentState!.validate()) {
              setState(() {
                _loading = true;
              });
              final name = nameController.text;
              final categoryId = await categoryViewModel.createCategory(name,
                  colorPickerViewModel.color.toHex());
              if (categoryId != null && mounted) {
                Navigator.of(context).pop(categoryId);
              } else {
                context.showErrorSnackBar(message: 'Failed to save, try again'
                    ' later');
                setState(() {
                  _loading = false;
                });
              }
            } else {
              setState(() {
                _loading = false;
              });
              context.showErrorSnackBar(message: 'Please fix the form');
            }
          },
          child: _loading ? const Text('...') : const Text('Create'),
        ),
        TextButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          child: const Text('Cancel'),
        )
      ],
    );
  }
}
