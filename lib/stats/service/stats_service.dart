import 'dart:convert';

import 'package:time_since/service/supabase_service.dart';
import 'package:time_since/stats/models/stat_model.dart';
import 'package:time_since/times/models/time_model.dart';

class StatsService extends SupabaseService {
  StatsService(super.client);

  Future<List<StatModel>> getStats() async {
    final user = await getUser();
    if (user != null) {
      final res = await client.from('history').select('''
          id, times_id, times (name), created_by, created_at, history
          ''').eq('created_by', user.id).execute();
      if (res.hasError) {
        final errorMessage = res.error.toString();
        throw Exception(errorMessage);
      } else if (res.data != null) {
        return _parseData(res.data);
      }
    }
    throw Exception('No user');
  }

  Future<void> updateStatsForTime(TimeModel time) async {
    final user = await getUser();
    if (user != null) {
      final res = await client
          .from('history')
          .select()
          .eq('times_id', time.id)
          .execute();
      if (res.hasError) {
        final errorMessage = res.error.toString();
        throw Exception(errorMessage);
      } else if (res.data != null) {
        if (res.data.length != 0) {
          final data = res.data[0];
          final List<dynamic> history = jsonDecode(data['history']);
          history.add({
            'value': time.timestamp.toLocal().toIso8601String(),
            'updated_at': DateTime.now().toUtc().toIso8601String()
          });
          final updateRes = await client
              .from('history')
              .update({
                'history': jsonEncode(history),
              })
              .eq('id', data['id'])
              .execute();
          if (updateRes.hasError) {
            final errorMessage = updateRes.error.toString();
            throw Exception(errorMessage);
          }
        } else {
          final history = {
            'value': time.timestamp.toLocal().toIso8601String(),
            'updated_at': DateTime.now().toUtc().toIso8601String()
          };
          final res = await client.from('history').insert({
            'created_by': user.id,
            'history': jsonEncode([history]),
            'times_id': time.id
          }).execute();
          if (res.hasError) {
            final errorMessage = res.error.toString();
            throw Exception(errorMessage);
          }
        }
      }
    } else {
      throw Exception('No user');
    }
  }

  List<StatModel> _parseData(List<dynamic> data) {
    return data.map((e) => StatModel.fromJson(e)).toList();
  }
}
