import 'package:flutter/material.dart';
import 'package:time_since/models/loading_status.dart';
import 'package:time_since/stats/models/stat_model.dart';
import 'package:time_since/stats/service/stats_service.dart';
import 'package:time_since/times/models/time_model.dart';
import 'package:time_since/view_models/base_view_model.dart';

class StatsViewModel extends ChangeNotifier with BaseViewModel {
  StatsViewModel(this.statsService);

  final StatsService statsService;

  LoadingStatus loadingStatus = LoadingStatus.initial('No stats to view');
  List<StatModel> stats = List.empty(growable: true);

  DateTime _lastUpdate = DateTime(1990);

  Future<void> getStats() async {
    loadingStatus = LoadingStatus.loading('Fetching stats');
    notifyListeners();
    try {
      stats = await statsService.getStats();
      loadingStatus = LoadingStatus.completed();
      _lastUpdate = DateTime.now();
      notifyListeners();
    } catch (err) {
      debugPrint(err.toString());
      loadingStatus = LoadingStatus.error('Failed to get stats, try again '
          'later');
      notifyListeners();
    }
  }

  Future<void> updateStatsForTime(TimeModel time) async {
    loadingStatus = LoadingStatus.loading('Fetching stats');
    notifyListeners();
    try {
      await statsService.updateStatsForTime(time);
      loadingStatus = LoadingStatus.completed();
      _lastUpdate = DateTime(1990);
      notifyListeners();
    } catch (err) {
      debugPrint(err.toString());
      loadingStatus = LoadingStatus.error('Failed to update stats, try again '
          'later');
      notifyListeners();
    }
  }

  Future<void> refreshStats() async {
    await refresh(_lastUpdate, getStats);
  }
}
