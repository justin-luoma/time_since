import 'dart:convert';

class StatModel {
  final int id;
  final int timesId;
  final String createdBy;
  final DateTime createdAt;
  final String name;
  final List<Map<String, DateTime>> history;

  StatModel(
      {required this.id,
      required this.timesId,
      required this.createdBy,
      required this.createdAt,
      required this.name,
      required this.history});

  factory StatModel.fromJson(Map<String, dynamic> json) {
    final List<dynamic> history = jsonDecode(json['history']);
    return StatModel(
      id: json['id'],
      timesId: json['times_id'],
      createdBy: json['created_by'],
      createdAt: DateTime.parse(json['created_at']),
      name: json['times']['name'],
      history: history
          .map((e) => {
                'value': DateTime.parse(e['value']),
                'updatedAt': DateTime.parse(e['updated_at'])
              })
          .toList(),
    );
  }

  @override
  String toString() {
    return 'Stat{id: $id, timesId: $timesId, createdBy: $createdBy, createdAt: $createdAt, name: $name, history: $history}';
  }
}
