import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:time_since/components/auth/auth_required_state.dart';
import 'package:time_since/models/loading_status.dart';
import 'package:time_since/stats/models/stat_model.dart';
import 'package:time_since/stats/view_models/stats_view_model.dart';
import 'package:time_since/utils/time.dart';

class StatsScreen extends StatefulWidget {
  const StatsScreen(this.context, {Key? key}) : super(key: key);

  final BuildContext context;

  @override
  _StatsScreenState createState() => _StatsScreenState();
}

class _StatsScreenState extends AuthRequiredState<StatsScreen> {
  @override
  Widget build(BuildContext context) {
    final vm = Provider.of<StatsViewModel>(context);

    return SafeArea(
      child: vm.loadingStatus.status == Status.loading
          ? const Center(child: CircularProgressIndicator())
          : RefreshIndicator(
              onRefresh: () async {
                await vm.getStats();
              },
              child: ListView(
                padding: const EdgeInsets.symmetric(
                  vertical: 18,
                  horizontal: 12,
                ),
                children: vm.stats.isNotEmpty ? _buildStatCards(vm.stats) : [
                  Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: const [
                            Padding(
                              padding: EdgeInsets.only(top: 18.0),
                              child: Text('No stats'),
                            )
                          ],
                        )
                      ],
              ),
            ),
    );
  }

  List<Widget> _buildStatCards(List<StatModel> stats) {
    List<Widget> widgets = List.empty(growable: true);

    for (final stat in stats) {
      widgets.add(Card(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Expanded(
                    child: Text(
                      stat.name,
                      style: const TextStyle(fontSize: 24),
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                ],
              ),
              Column(children: _buildHistoryRows(stat.history))
            ],
          ),
        ),
      ));
    }

    return widgets;
  }

  List<Widget> _buildHistoryRows(List<Map<String, DateTime>> history) {
    final List<Widget> widgets = List.empty(growable: true);
    for (final stat in history) {
      widgets.add(Row(
        children: [
          Text(
            'Reset: ${formatDate(stat['updatedAt']!)}, Value: '
            '${formatDate(stat['value']!)}',
            overflow: TextOverflow.ellipsis,
          )
        ],
      ));
    }

    return widgets;
  }

  @override
  void initState() {
    super.initState();
    Future.microtask(() =>
        Provider.of<StatsViewModel>(context, listen: false).refreshStats());
  }
}
