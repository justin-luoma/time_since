

abstract class BaseViewModel {
  Future<void> refresh(DateTime lastCalled, Function callback) async {
    final now = DateTime.now();
    if (now.difference(lastCalled).inMinutes > 5) {
      await callback();
    }
  }
}
