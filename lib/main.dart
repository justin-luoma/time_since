import 'dart:async';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_background_service/flutter_background_service.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:flutter_native_timezone/flutter_native_timezone.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:time_since/cache/service/cache_service.dart';
import 'package:time_since/cache/service/supabase_file_service.dart';
import 'package:time_since/category/service/category_service.dart';
import 'package:time_since/category/view_models/category_view_model.dart';
import 'package:time_since/config.dart' as local;
import 'package:time_since/pages/account_page.dart';
import 'package:time_since/pages/login_page.dart';
import 'package:time_since/pages/main_page.dart';
import 'package:time_since/pages/reset_password_page.dart';
import 'package:time_since/pages/settings_page.dart';
import 'package:time_since/pages/splash_page.dart';
import 'package:time_since/service/notifier_service.dart';
import 'package:time_since/stats/service/stats_service.dart';
import 'package:time_since/stats/stats_screen.dart';
import 'package:time_since/stats/view_models/stats_view_model.dart';
import 'package:time_since/times/screens/times_screen.dart';
import 'package:time_since/times/service/times_service.dart';
import 'package:time_since/times/view_models/times_view_model.dart';
import 'package:timezone/data/latest_all.dart' as tz;
import 'package:timezone/timezone.dart' as tz;

final notifierService = NotifierService.instance;

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await _configureLocalTimeZone();

  final supabase = await _initSupabase();

  _waitForUser(supabase.client);

  Map<String, Widget Function(BuildContext)> screens = {
    'Home': (context) => TimesScreen(context),
    'Stats': (context) => StatsScreen(context),
  };

  final statsService = StatsService(supabase.client);
  final timesService = TimesService(supabase.client);

  final cacheManager = CacheManager(Config('supabaseCache',
      fileService: SupabaseFileService(supabase.client)));

  final cacheService = CacheService(cacheManager);
  final categoryService = CategoryService(supabase.client);

  runApp(MyApp(
    screens,
    statsService: statsService,
    timesService: timesService,
    cacheService: cacheService,
    categoryService: categoryService,
  ));
}

Future<void> _waitForUser(SupabaseClient supabase) async {
  while (supabase.auth.user() == null) {
    debugPrint('waiting for user');
    await Future.delayed(const Duration(minutes: 2));
  }

  await notifierService.init();

  await _initializeService();
}

Future<void> _initializeService() async {
  final service = FlutterBackgroundService();
  await service.configure(
    iosConfiguration: IosConfiguration(
      autoStart: true,
      onForeground: _startBackgroundService,
      onBackground: _onIosBackground,
    ),
    androidConfiguration: AndroidConfiguration(
      autoStart: true,
      onStart: _startBackgroundService,
      isForegroundMode: true,
      foregroundServiceNotificationTitle: 'Background Service',
      foregroundServiceNotificationContent: 'Processing',
    ),
  );
  service.startService();
}

class MyApp extends StatelessWidget {
  const MyApp(this.screens,
      {Key? key,
      required this.statsService,
      required this.timesService,
      required this.cacheService,
      required this.categoryService})
      : super(key: key);

  final Map<String, Widget Function(BuildContext)> screens;
  final StatsService statsService;
  final TimesService timesService;
  final CacheService cacheService;
  final CategoryService categoryService;

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider(
            create: (_) => StatsViewModel(statsService),
          ),
          ChangeNotifierProvider(
            create: (_) => TimesViewModel(timesService, cacheService, categoryService),
          ),
          ChangeNotifierProvider(
            create: (_) => CategoryViewModel(categoryService, cacheService),
          ),
        ],
        child: MaterialApp(
          title: 'Time Since',
          theme: ThemeData.dark().copyWith(
            textTheme: GoogleFonts.playfairDisplayTextTheme(ThemeData.dark().textTheme),
            primaryColor: Colors.indigo,
            elevatedButtonTheme: ElevatedButtonThemeData(
              style: ElevatedButton.styleFrom(
                onPrimary: Colors.white,
                primary: Colors.indigo,
              ),
            ),
          ),
          initialRoute: '/',
          routes: <String, WidgetBuilder>{
            '/': (_) => const SplashPage(),
            '/login': (_) => const LoginPage(),
            '/account': (_) => const AccountPage(),
            '/reset': (_) => const ResetPasswordPage(),
            '/settings': (_) => const SettingsPage(),
            '/main': (_) => MainPage(screens: screens)
          },
        ));
  }
}

Future<Supabase> _initSupabase() async {
  return await Supabase.initialize(
    url: local.Config.supabaseURL,
    anonKey: local.Config.supabaseAnonPublicKey,
  );
}

Future<void> _configureLocalTimeZone() async {
  if (kIsWeb || Platform.isLinux) {
    return;
  }
  tz.initializeTimeZones();
  final String timeZoneName = await FlutterNativeTimezone.getLocalTimezone();
  tz.setLocalLocation(tz.getLocation(timeZoneName));
}

Future<void> _startBackgroundService(ServiceInstance service) async {
  return notifierService.startBackgroundService(service);
}

FutureOr<bool> _onIosBackground(ServiceInstance service) {
  return notifierService.onIosBackground(service);
}
