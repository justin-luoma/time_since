import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:time_since/components/auth/auth_required_state.dart';
import 'package:time_since/times/pages/time_update_page.dart';
import 'package:time_since/times/view_models/times_view_model.dart';

class MainPage extends StatefulWidget {
  const MainPage({super.key, required this.screens});

  final Map<String, Widget Function(BuildContext)> screens;

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends AuthRequiredState<MainPage>
    with TickerProviderStateMixin {
  late final TabController _tabController;
  int _currentIndex = 0;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 2, vsync: this);
    Future.microtask(() {
      Provider.of<TimesViewModel>(context, listen: false).updateCurrentUser();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: _bottomNavigationBar(),
      body: SafeArea(
        child: _currentIndex == 0
            ? ListenableProvider.value(
                value: _tabController,
                child:
                    widget.screens.values.elementAt(_currentIndex)(context),
              )
            : widget.screens.values.elementAt(_currentIndex)(context),
      ),
      appBar: AppBar(
        title: const Text('Time Since'),
        bottom: _currentIndex == 0
            ? TabBar(
                controller: _tabController,
                tabs: const [
                  Tab(
                    text: 'Categories',
                  ),
                  Tab(
                    text: 'All',
                  ),
                ],
              )
            : null,
        actions: [
          IconButton(
            onPressed: () {
              final vm = Provider.of<TimesViewModel>(context, listen: false);
              vm.timeBeingEdited = null;
              vm.imageBeingUpdated = null;
              Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => const TimeUpdatePage()
                      // ChangeNotifierProvider.value(
                      //   value: timesViewModel,
                      //   child: const CreateTimePage(),
                      // )
                      ));
            },
            icon: const Icon(Icons.add),
          ),
          PopupMenuButton<String>(
            itemBuilder: (context) {
              return [
                PopupMenuItem<String>(
                  value: '/settings',
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: const [
                      Padding(
                        padding: EdgeInsets.only(right: 8.0),
                        child: Icon(Icons.settings),
                      ),
                      Text('Settings')
                    ],
                  ),
                )
              ];
            },
            onSelected: (route) => Navigator.of(context).pushNamed(route),
          ),
        ],
      ),
    );
  }

  BottomNavigationBar _bottomNavigationBar() {
    return BottomNavigationBar(
      elevation: 2,
      currentIndex: _currentIndex,
      onTap: (index) {
        if (_currentIndex != index) {
          setState(() {
            _currentIndex = index;
          });
        }
      },
      items: [
        BottomNavigationBarItem(
          icon: const Icon(Icons.access_time),
          label: widget.screens.keys.elementAt(0),
        ),
        BottomNavigationBarItem(
          icon: const Icon(Icons.bar_chart),
          label: widget.screens.keys.elementAt(1),
        ),
      ],
    );
  }
}
