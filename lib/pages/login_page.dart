import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:supabase/supabase.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:time_since/components/auth/auth_state.dart';
import 'package:time_since/config.dart';
import 'package:time_since/utils/constants.dart';
import 'package:encrypt/encrypt.dart' as crypto;

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends AuthState<LoginPage> {
  late final TextEditingController _emailController;
  late final TextEditingController _passwordController;

  SharedPreferences? _preferences;

  bool _isLoading = false;

  Future<void> _signInWithMagicLink() async {
    setState(() {
      _isLoading = true;
    });
    final response = await supabase.auth.signIn(
        email: _emailController.text,
        options: AuthOptions(
            redirectTo:
                kIsWeb ? null : 'dev.luoma.time-since://login-callback/'));
    final error = response.error;
    if (error != null) {
      context.showErrorSnackBar(message: error.message);
    } else {
      context.showSnackBar(message: 'Check your email for login link!');
      await _saveLogin();
      _emailController.clear();
      _passwordController.clear();
    }

    setState(() {
      _isLoading = false;
    });
  }

  Future<void> _signInWithPassword() async {
    setState(() {
      _isLoading = true;
    });
    final response = await supabase.auth.signIn(
      email: _emailController.text,
      password: _passwordController.text,
    );
    final error = response.error;
    if (error != null) {
      context.showErrorSnackBar(message: error.message);
    } else if (response.data != null) {
      await _saveLogin();
      onAuthenticated(response.data!);
      _emailController.clear();
      _passwordController.clear();
    }

    setState(() {
      _isLoading = false;
    });
  }

  Future<void> _resetPassword() async {
    if (_emailController.text.isEmpty) {
      context.showErrorSnackBar(message: 'Enter your email');

      return;
    }

    final response = await supabase.auth.api.resetPasswordForEmail(
      _emailController.text,
      options: AuthOptions(
          redirectTo: kIsWeb ? null : 'dev.luoma.time-since://reset-callback/'),
    );

    final error = response.error;
    if (error != null) {
      context.showErrorSnackBar(message: error.message);
    } else {
      context.showSnackBar(message: 'Password reset link sent');
    }
  }

  @override
  void initState() {
    super.initState();
    _emailController = TextEditingController();
    _passwordController = TextEditingController();
    _initSharedPreferences();
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    const padding = EdgeInsets.symmetric(vertical: 18, horizontal: 12);
    return Scaffold(
      appBar: AppBar(title: const Text('Sign In')),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Flexible(
            child: ListView(
              shrinkWrap: true,
              padding: padding,
              children: [
                const Text('Sign in or sign up via the magic link with your '
                    'email, or '
                    'use your email and password'),
                const SizedBox(height: 18),
                TextFormField(
                  controller: _emailController,
                  decoration: const InputDecoration(labelText: 'Email'),
                  inputFormatters: [FilteringTextInputFormatter.deny(' ')],
                ),
                const SizedBox(height: 18),
                ElevatedButton(
                  onPressed: _isLoading ? null : _signInWithMagicLink,
                  child: Text(_isLoading ? 'Loading' : 'Login with magic link'),
                ),
                TextFormField(
                  controller: _passwordController,
                  decoration: const InputDecoration(labelText: 'Password'),
                  obscureText: true,
                ),
                const SizedBox(height: 18),
                ElevatedButton(
                  onPressed: _isLoading ? null : _signInWithPassword,
                  child: Text(_isLoading ? 'Loading' : 'Login with password'),
                ),
                const SizedBox(height: 18),
              ],
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.only(left: 18, right: 18, top: 18, bottom: 27),
            child: SizedBox(
              width: double.infinity,
              child: ElevatedButton(
                onPressed: _isLoading ? null : _resetPassword,
                child: Text(_isLoading ? 'Loading' : 'Reset Password'),
              ),
            ),
          ),
        ],
      ),
    );
  }

  void _initSharedPreferences() async {
    final preferences = await SharedPreferences.getInstance();
    final email = preferences.getString(Config.emailKey);
    final encryptedPass = preferences.getString(Config.passKey);
    final cryptoBase64 = preferences.getString(Config.cryptoKey);
    final ivBase64 = preferences.getString(Config.ivKey);

    String? pass;

    if (cryptoBase64 != null && encryptedPass != null && ivBase64 != null) {
      final key = crypto.Key.fromBase64(cryptoBase64);
      final iv = crypto.IV.fromBase64(ivBase64);

      final encrypter = crypto.Encrypter(crypto.AES(key));

      pass = encrypter.decrypt64(encryptedPass, iv: iv);
    }
    setState(() {
      _preferences = preferences;
      if (email != null) {
        _emailController.text = email;
      }
      if (pass != null) {
        _passwordController.text = pass;
      }
    });
  }

  Future<void> _saveLogin() async {
    if (_preferences != null) {
      if (_emailController.text.isNotEmpty) {
        await _preferences!.setString(Config.emailKey, _emailController.text);
      }
      if (_passwordController.text.isNotEmpty) {
        final key = crypto.Key.fromSecureRandom(32);
        final iv = crypto.IV.fromSecureRandom(16);
        final keyBase64 = base64.encode(key.bytes);
        final ivBase64 = base64.encode(iv.bytes);
        await Future.wait([
          _preferences!.setString(Config.cryptoKey, keyBase64),
          _preferences!.setString(Config.ivKey, ivBase64)
        ]);
        final encrypter = crypto.Encrypter(crypto.AES(key));
        final encryptedPass = encrypter.encrypt(_passwordController.text, iv: iv);
        final encryptedPassBase64 = base64.encode(encryptedPass.bytes);
        await _preferences!.setString(Config.passKey, encryptedPassBase64);
      }
    }
  }
}
