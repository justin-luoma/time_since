import 'package:flutter/material.dart';
import 'package:time_since/components/auth/auth_required_state.dart';
import 'package:time_since/screens/reports_screen.dart';
import 'package:time_since/utils/constants.dart';

class SettingsPage extends StatefulWidget {
  const SettingsPage({Key? key}) : super(key: key);

  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends AuthRequiredState<SettingsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Settings'),
      ),
      body: SafeArea(
        child: Column(
          children: [
            Flexible(
              child: ListView(
                padding: const EdgeInsets.symmetric(
                  vertical: 18,
                  horizontal: 12,
                ),
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ElevatedButton(
                        onPressed: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => ReportsScreen()));
                        },
                        child: const Text('Report'),
                      )
                    ],
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ElevatedButton(
                    onPressed: () async {
                      await supabase.auth.signOut();
                    },
                    style: ElevatedButton.styleFrom(primary: Colors.red),
                    child: const Text('Sign Out'),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
