import 'dart:typed_data';

class CachedImageModel {
  final Uint8List image;
  final String imageUrl;

  CachedImageModel(this.image, this.imageUrl);
}
