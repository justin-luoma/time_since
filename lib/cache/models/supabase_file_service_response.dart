import 'dart:typed_data';

import 'package:flutter_cache_manager/flutter_cache_manager.dart';

class SupabaseFileServiceResponse implements FileServiceResponse {
  SupabaseFileServiceResponse(this.file, this.filePath);

  final Uint8List file;
  final String filePath;
  final DateTime expires = DateTime.now().add(const Duration(days: 5));

  @override
  Stream<List<int>> get content => Stream.fromIterable(Iterable.castFrom(file));

  @override
  int? get contentLength => file.length;

  @override
  String? get eTag => null;

  @override
  String get fileExtension => filePath.split('.').last;

  @override
  int get statusCode => 200;

  @override
  DateTime get validTill => expires;

}
