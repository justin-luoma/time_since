import 'dart:typed_data';

import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:time_since/cache/models/cached_image_model.dart';

class CacheService {
  CacheService(this.cacheManager);

  final CacheManager cacheManager;

  Future<void> putImage(CachedImageModel cachedImage) async {
    await cacheManager.putFile(cachedImage.imageUrl, cachedImage.image);
  }

  Future<Uint8List?> getImage(String imageUrl) async {
    final FileInfo? fileInfo = await cacheManager.getFileFromCache(imageUrl);
    return fileInfo?.file.readAsBytes();
  }

  Future<void> removeImage(String imageUrl) async {
    await cacheManager.removeFile(imageUrl);
  }
}
