import 'dart:typed_data';

import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:storage_client/src/types.dart';
import 'package:time_since/cache/models/supabase_file_service_response.dart';
import 'package:time_since/service/supabase_service.dart';

class SupabaseFileService extends SupabaseService with FileService {
  SupabaseFileService(super.client);

  @override
  Future<FileServiceResponse> get(String url, {Map<String, String>? headers})
  async {
    final user = getUser();
    if (user != null) {
      final StorageResponse<Uint8List> res = await client.storage.from('images').download(url);
      if (res.hasError) {
        final errorMessage = res.error.toString();
        throw Exception(errorMessage);
      } else if (res.data != null) {
        final fileResponse = SupabaseFileServiceResponse(res.data!, url);
        return fileResponse;
      } else {
        throw Exception('No image found');
      }
    } else {
      throw Exception('No user');
    }
  }

}
