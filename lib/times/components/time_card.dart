import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:time_since/colors/extensions/color.dart';
import 'package:time_since/stats/view_models/stats_view_model.dart';
import 'package:time_since/times/models/time_model.dart';
import 'package:time_since/times/pages/time_update_page.dart';
import 'package:time_since/times/screens/image_display_screen.dart';
import 'package:time_since/times/view_models/times_view_model.dart';
import 'package:time_since/utils/time.dart';

class TimeCard extends StatelessWidget {
  const TimeCard(this.time, this.open, this.setOpen, {Key? key})
      : super(key: key);

  final bool open;
  final Function(bool) setOpen;
  final TimeModel time;

  @override
  Widget build(BuildContext context) {
    final time = Provider.of<TimesViewModel>(context).times[this.time.id]!;
    Future<void> _showImageDisplayScreen(Uint8List currentImage) async {
      await Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => ImageDisplayScreen(image: currentImage)));
    }

    _deleteTime(TimeModel time) async {
      final vm = Provider.of<TimesViewModel>(context, listen: false);
      await vm.deleteTime(time);
    }

    _resetTime(TimeModel time) async {
      final timesViewModel =
          Provider.of<TimesViewModel>(context, listen: false);
      final statsViewModel =
          Provider.of<StatsViewModel>(context, listen: false);
      // final categoryViewModel =
      //     Provider.of<CategoryViewModel>(context, listen: false);
      await Future.wait([
        timesViewModel.resetTime(time),
        statsViewModel.updateStatsForTime(time)
      ]);
    }

    _confirmReset(TimeModel time) {
      showDialog(
          context: context,
          builder: (context) {
            return AlertDialog(
              title: Text('Resetting ${time.name}'),
              content: const Text('Are you sure?'),
              actions: [
                TextButton(
                  onPressed: () {
                    _resetTime(time);
                    Navigator.of(context).pop();
                  },
                  child: const Text('Yes'),
                ),
                TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: const Text('No'),
                )
              ],
            );
          });
    }

    _confirmDelete(TimeModel time) {
      showDialog(
          context: context,
          builder: (context) {
            return AlertDialog(
              title: Text('Deleting ${time.name}'),
              content: const Text('Are you sure?'),
              actions: [
                TextButton(
                  onPressed: () {
                    _deleteTime(time);
                    Navigator.of(context).pop();
                  },
                  child: const Text('Yes'),
                ),
                TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: const Text('No'),
                )
              ],
            );
          });
    }

    Row _buttonRow(TimeModel time) {
      return Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          ElevatedButton(
            onPressed: () => _confirmDelete(time),
            style: ElevatedButton.styleFrom(
              primary: Colors.pink,
            ),
            child: const Text('Delete'),
          ),
          ElevatedButton(
            onPressed: () => _confirmReset(time),
            style: ElevatedButton.styleFrom(
              primary: Colors.redAccent,
            ),
            child: const SizedBox(
              height: 60,
              child: Center(
                child: Text(
                  'Reset',
                  style: TextStyle(fontSize: 18),
                ),
              ),
            ),
          ),
        ],
      );
    }

    Row _nameRow(TimeModel time, BuildContext context) {
      return Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Flexible(
            fit: FlexFit.tight,
            child: FittedBox(
              alignment: Alignment.centerLeft,
              fit: BoxFit.scaleDown,
              child: Text(
                time.name,
                style: const TextStyle(fontSize: 30),
              ),
            ),
          ),
          SizedBox(
            height: 24,
            width: 30,
            child: IconButton(
              onPressed: () async {
                Provider.of<TimesViewModel>(context, listen: false)
                    .timeBeingEdited = time;
                await Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => const TimeUpdatePage(),
                ));
              },
              icon: const Icon(
                Icons.edit,
                size: 24,
              ),
              padding: const EdgeInsets.fromLTRB(8, 0, 0, 0),
            ),
          ),
        ],
      );
    }

    final backgroundColor = time.category != null
        ? HexColor.fromHex(time.category!.color)
        : Theme.of(context).appBarTheme.foregroundColor;

    return GestureDetector(
      onTap: () {
        setOpen(!open);
      },
      child: Card(
        shape: const RoundedRectangleBorder(
          side: BorderSide(
            color: Colors.black54,
          ),
          borderRadius: BorderRadius.all(Radius.circular(12)),
        ),
        color: isDue(time)
            ? const Color.fromRGBO(181, 63, 81, 100.0)
            : backgroundColor,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Flex(
            direction: Axis.horizontal,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: Container(
                  margin: const EdgeInsets.all(10),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Flexible(
                        child: _nameRow(time, context),
                      ),
                      if (time.image != null)
                        Row(
                          children: [
                            Padding(
                              padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
                              child: InkWell(
                                onTap: () =>
                                    _showImageDisplayScreen(time.image!),
                                child: Image.memory(
                                  time.image!,
                                  height: 100,
                                  width: 100,
                                  fit: BoxFit.contain,
                                  alignment: Alignment.centerLeft,
                                ),
                              ),
                            ),
                          ],
                        ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: FittedBox(
                              alignment: Alignment.centerLeft,
                              fit: BoxFit.scaleDown,
                              child: Text(
                                getDateAndFrequency(time),
                                style: const TextStyle(fontSize: 20),
                              ),
                            ),
                          ),
                          // SizedBox(
                          //   height: 18,
                          //   width: 18,
                          //   child: IconButton(
                          //     onPressed: () =>
                          //         _showDatePicker(context, time),
                          //     icon: const Icon(
                          //       Icons.edit,
                          //       size: 16,
                          //     ),
                          //     padding:
                          //     const EdgeInsets.fromLTRB(8, 0, 0, 0),
                          //   ),
                          // ),
                          Padding(
                            padding: const EdgeInsets.only(left: 8.0),
                            child: Text(
                                style: const TextStyle(fontSize: 24),
                                timeSince(time.timestamp)),
                          )
                        ],
                      ),
                      open
                          ? Padding(
                              padding: const EdgeInsets.only(top: 8.0),
                              child: _buttonRow(time),
                            )
                          : const SizedBox(
                              height: 5,
                            ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
