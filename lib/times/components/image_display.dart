import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:time_since/times/screens/image_editor_screen.dart';

class ImageDisplay extends StatelessWidget {
  const ImageDisplay({
    Key? key,
    required this.imageBytes,
    required this.newImageSelected,
  }) : super(key: key);

  final Uint8List? imageBytes;
  final void Function(String, Uint8List) newImageSelected;

  @override
  Widget build(BuildContext context) {
    Future<void> _pickImage(bool useCamera) async {
      final picker = ImagePicker();
      final imageFile = await picker.pickImage(
        source: useCamera ? ImageSource.camera : ImageSource.gallery,
      );
      if (imageFile == null) {
        return;
      }

      final bytes = await imageFile.readAsBytes();
      final fileExt = imageFile.path.split('.').last;
      final fileName = '${DateTime.now().toIso8601String()}.$fileExt';

      final Uint8List? croppedImage = await Navigator.of(context).push(
          MaterialPageRoute(
              builder: (context) => ImageEditorScreen(image: bytes)));

      if (croppedImage != null) {
        debugPrint("***** cropped image ******");

        newImageSelected(fileName, croppedImage);
      } else {
        debugPrint("***** no cropped image ******");
      }
    }

    return Column(
      children: [
        if (imageBytes == null)
          Container(
            width: 300,
            height: 300,
            color: Colors.grey,
            child: const Center(
              child: Text('No Image'),
            ),
          )
        else
          Image.memory(
            imageBytes!,
            width: 300,
            height: 300,
            fit: BoxFit.contain,
          ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            ElevatedButton(
              onPressed: () => _pickImage(false),
              style: ElevatedButton.styleFrom(primary: Colors.indigoAccent),
              child: const Text('Select from Photos'),
            ),
            ElevatedButton(
              onPressed: () => _pickImage(true),
              style: ElevatedButton.styleFrom(primary: Colors.indigoAccent),
              child: const Text('Use Camera'),
            )
          ],
        ),
      ],
    );
  }
}

// Future<void> _finishedCropping(Uint8List? image) async {
//   if (image != null && _filePath != null) {
//     setState(() => _image = image);
//     await _completeUpload(_filePath!, image);
//   } else {
//     setState(() {
//       _isLoading = false;
//     });
//   }
// }
//
// Future<void> _completeUpload(String filePath, Uint8List bytes) async {
//   // await _uploadImage(filePath, bytes);
//   widget.onUpload(filePath, bytes);
// }
//
// Future<void> _uploadImage(String filePath, Uint8List bytes) async {
//   final response =
//       await supabase.storage.from('images').uploadBinary(filePath, bytes);
//
//   setState(() => _isLoading = false);
//
//   final error = response.error;
//   if (error != null) {
//     context.showErrorSnackBar(message: error.message);
//     return;
//   }
//   // final imageUrlResponse =
//   //     await supabase.storage.from('images').createSignedUrl(filePath, 3600);
//   // if (imageUrlResponse.hasError) {
//   //   debugPrint(jsonEncode(imageUrlResponse.error));
//   // } else {
//   //   widget.onUpload(imageUrlResponse.data!, filePath);
//   // }
// }
// }
