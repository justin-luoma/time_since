
import 'dart:typed_data';

class CreateTimeModel {
  final String name;
  final DateTime timestamp;
  final String? description;
  final String? imageUrl;
  final int? frequency;
  Uint8List? image;

  CreateTimeModel(this.name, this.timestamp,
      this.description, this.imageUrl, this.frequency, {this.image});
}
