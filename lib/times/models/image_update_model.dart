import 'dart:typed_data';

class ImageUpdateModel {
  final Uint8List image;
  final String imageUrl;

  ImageUpdateModel(this.image, this.imageUrl);
}
