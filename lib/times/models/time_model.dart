import 'dart:typed_data';

import 'package:time_since/category/models/category_model.dart';

class TimeModel {
  final int id;
  final String createdBy;
  final DateTime createdAt;
  final String name;
  final DateTime timestamp;
  final String? description;
  final String? imageUrl;
  final int? frequency;
  final DateTime? dueDate;
  CategoryModel? category;
  Uint8List? image;

  TimeModel(this.id, this.createdBy, this.createdAt, this.name, this.timestamp,
      this.description, this.imageUrl, this.frequency,
      {this.image, this.category, this.dueDate});

  factory TimeModel.fromJson(Map<String, dynamic> json) {
    final categoryJson = json['category'];
    CategoryModel? category;
    if (categoryJson != null && categoryJson.runtimeType != int) {
      category = CategoryModel.fromJson(categoryJson);
    }
    final timestamp = DateTime.parse(json['timestamp']);
    final int? frequency = json['frequency'];
    DateTime? dueDate;
    if (frequency != null) {
      dueDate = timestamp.add(Duration(days: frequency));
    }
    return TimeModel(
      json['id'],
      json['created_by'],
      DateTime.parse(json['created_at']),
      json['name'],
      timestamp,
      json['description'],
      json['image_url'],
      frequency,
      category: category,
      dueDate: dueDate,
    );
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is TimeModel && runtimeType == other.runtimeType && id == other.id;

  @override
  int get hashCode => id.hashCode;

  @override
  String toString() {
    return 'TimeModel{id: $id, createdBy: $createdBy, createdAt: $createdAt, name: $name, timestamp: $timestamp, description: $description, imageUrl: $imageUrl, frequency: $frequency, dueDate: $dueDate, category: $category, image: $image}';
  }
}
