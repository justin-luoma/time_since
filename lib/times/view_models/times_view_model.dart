import 'dart:collection';
import 'dart:typed_data';

import 'package:flutter/foundation.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:time_since/cache/models/cached_image_model.dart';
import 'package:time_since/cache/service/cache_service.dart';
import 'package:time_since/category/service/category_service.dart';
import 'package:time_since/models/loading_status.dart';
import 'package:time_since/times/models/create_time_model.dart';
import 'package:time_since/times/models/image_update_model.dart';
import 'package:time_since/times/models/time_model.dart';
import 'package:time_since/times/service/times_service.dart';
import 'package:time_since/view_models/base_view_model.dart';

class TimesViewModel extends ChangeNotifier with BaseViewModel {
  TimesViewModel(this.timesService, this.cacheService, this.categoryService);

  final TimesService timesService;
  final CacheService cacheService;
  final CategoryService categoryService;

  LoadingStatus loadingStatus = LoadingStatus.initial('No items to view');

  Map<int, TimeModel> times = Map.identity();
  DateTime _lastUpdate = DateTime(1990);

  User? currentUser;
  TimeModel? timeBeingEdited;
  ImageUpdateModel? imageBeingUpdated;

  Future<void> getTimes() async {
    loadingStatus = LoadingStatus.loading('Fetching times');
    notifyListeners();
    try {
      final List<TimeModel> data = await timesService.getTimes();
      times = await _processTimes(data);
      loadingStatus = LoadingStatus.completed();
      _lastUpdate = DateTime.now();
      notifyListeners();
    } catch (err) {
      debugPrint(err.toString());
      loadingStatus = LoadingStatus.error('Failed to get times, try again '
          'later');
      notifyListeners();
    }
  }

  Iterable<TimeModel> getTimesInCategory(int categoryId) {
    return times.values.where((time) => time.category?.id == categoryId);
  }

  Future<void> updateTime(int id, Map<String, dynamic> update,
      {ImageUpdateModel? imageUpdate}) async {
    loadingStatus = LoadingStatus.loading('Updating');
    notifyListeners();
    try {
      TimeModel? updatedTime;
      if (imageUpdate != null) {
        update['image_url'] = imageUpdate.imageUrl;
        final cachedImage =
            CachedImageModel(imageUpdate.image, imageUpdate.imageUrl);
        final List<dynamic> results = await Future.wait([
          timesService.updateTime(id, update),
          timesService.uploadImage(imageUpdate),
          cacheService.putImage(cachedImage)
        ]);
        updatedTime = results[0];
        updatedTime?.image = imageUpdate.image;
      } else {
        updatedTime = await timesService.updateTime(id, update);
        updatedTime.image = times[id]?.image;
        if (update.containsKey('category') && update['category'] != null) {
          updatedTime.category = times[id]?.category;
        }
      }
      times.update(id, (value) => updatedTime!);
      timeBeingEdited = updatedTime;
      loadingStatus = LoadingStatus.completed();
      notifyListeners();
    } catch (err) {
      debugPrint(err.toString());
      loadingStatus =
          LoadingStatus.error("Failed to update, please refresh and try again");
      notifyListeners();
      rethrow;
    }
  }

  Future<void> createTime(CreateTimeModel createTime) async {
    loadingStatus = LoadingStatus.loading('Creating');
    notifyListeners();
    try {
      TimeModel? time;
      if (createTime.image != null && createTime.imageUrl != null) {
        final imageUpdate =
            ImageUpdateModel(createTime.image!, createTime.imageUrl!);
        final cachedImage =
            CachedImageModel(imageUpdate.image, imageUpdate.imageUrl);
        final List<dynamic> results = await Future.wait([
          timesService.uploadImage(imageUpdate),
          timesService.createTime(createTime),
          cacheService.putImage(cachedImage)
        ]);
        time = results[1];
        time?.image = createTime.image;
      } else {
        time = await timesService.createTime(createTime);
      }
      times.update(time!.id, (value) => time!, ifAbsent: () => time!);
      loadingStatus = LoadingStatus.completed();
      notifyListeners();
    } catch (err) {
      debugPrint('createTime error: ${err.toString()}');
      loadingStatus =
          LoadingStatus.error("Failed to save, please refresh and try again");
      notifyListeners();
    }
  }

  Future<void> deleteTime(TimeModel time) async {
    loadingStatus = LoadingStatus.loading('Deleting');
    notifyListeners();
    try {
      await timesService.deleteTime(time);
      if (time.imageUrl != null) {
        await cacheService.removeImage(time.imageUrl!);
      }
      times.remove(time.id);
      loadingStatus = LoadingStatus.completed();
      notifyListeners();
    } catch (err) {
      debugPrint('createTime error: ${err.toString()}');
      loadingStatus =
          LoadingStatus.error("Failed to delete, please refresh and try again");
      notifyListeners();
    }
  }

  Future<void> resetTime(TimeModel time) async {
    loadingStatus = LoadingStatus.loading('Resetting');
    notifyListeners();
    try {
      final now = DateTime.now().toUtc().toIso8601String();
      final update = {'timestamp': now};
      final updatedTime = await timesService.updateTime(time.id, update);
      updatedTime.image = time.image;
      updatedTime.category = time.category;
      times.update(time.id, (value) => updatedTime);
      loadingStatus = LoadingStatus.completed();
      notifyListeners();
    } catch (err) {
      debugPrint('resetTime error: ${err.toString()}');
      loadingStatus =
          LoadingStatus.error("Failed to reset, please refresh and try again");
      notifyListeners();
    }
  }

  Future<Map<int, TimeModel>> _processTimes(List<TimeModel> data) async {
    Map<int, TimeModel> timesMap = HashMap();
    for (final timeModel in data) {
      timesMap.update(
        timeModel.id,
        (value) => timeModel,
        ifAbsent: () => timeModel,
      );
      if (timeModel.imageUrl != null) {
        _getImageForTime(timeModel);
      }
    }
    return timesMap;
  }

  Future<void> refreshTimes() async {
    await refresh(_lastUpdate, getTimes);
    // final now = DateTime.now();
    // if (now.difference(_lastUpdate).inMinutes > 5 ||
    //     loadingStatus.status == Status.error) {
    //   await getTimes();
    // }
  }

  Future<void> _getImageForTime(TimeModel timeModel) async {
    if (timeModel.imageUrl != null) {
      try {
        Uint8List? image = await cacheService.getImage(timeModel.imageUrl!);
        if (image == null) {
          image = await timesService.getImage(timeModel.imageUrl!);
          _addImageToCache(timeModel.imageUrl!, image);
        }
        times.update(timeModel.id, (value) {
          value.image = image;
          return value;
        }, ifAbsent: () {
          timeModel.image = image;
          return timeModel;
        });
        notifyListeners();
      } catch (err) {
        debugPrint(err.toString());
      }
    }
  }

  Future<void> _addImageToCache(String imageUrl, Uint8List image) async {
    final cachedImage = CachedImageModel(image, imageUrl);
    await cacheService.putImage(cachedImage);
  }

  Future<void> updateImage(int timesId, ImageUpdateModel imageUpdate) async {
    try {
      final cachedImage =
          CachedImageModel(imageUpdate.image, imageUpdate.imageUrl);
      await Future.wait([
        timesService.uploadImage(imageUpdate),
        cacheService.putImage(cachedImage)
      ]);
      final update = {'image_url': imageUpdate.imageUrl};
      await timesService.updateTime(timesId, update);
      times.update(timesId, (value) {
        value.image = imageUpdate.image;
        return value;
      });
      notifyListeners();
    } catch (err) {
      debugPrint(err.toString());
      rethrow;
    }
  }

  Future<bool> removeImage(int timesId, String imageUrl) async {
    try {
      await timesService.removeImageUrlAndDeleteImage(timesId, imageUrl);
      await cacheService.removeImage(imageUrl);
      return true;
    } catch (err) {
      debugPrint(err.toString());
      return false;
    }
  }

  void setImageBeingUpdated(ImageUpdateModel? imageUpdate) {
    imageBeingUpdated = imageUpdate;
    notifyListeners();
  }

  void updateCurrentUser() async {
    currentUser = await timesService.getUser();
  }

// void updateTimeBeingEdited(String imageUrl, Uint8List image) {
//   if (timeBeingEdited != null) {
//     debugPrint('updating being edited');
//     TimeModel time = TimeModel(
//         timeBeingEdited!.id,
//         timeBeingEdited!.createdBy,
//         timeBeingEdited!.createdAt,
//         timeBeingEdited!.name,
//         timeBeingEdited!.timestamp,
//         timeBeingEdited!.description,
//         imageUrl,
//         timeBeingEdited!.frequency);
//     time.image = image;
//     timeBeingEdited = time;
//     notifyListeners();
//   }
// }
}
