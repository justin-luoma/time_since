import 'dart:typed_data';

import 'package:time_since/service/supabase_service.dart';
import 'package:time_since/times/models/create_time_model.dart';
import 'package:time_since/times/models/image_update_model.dart';
import 'package:time_since/times/models/time_model.dart';

class TimesService extends SupabaseService {
  TimesService(super.client);

  Future<List<TimeModel>> getTimes() async {
    final user = await getUser();
    if (user != null) {
      final res = await client
          .from('times')
          .select('''
          *, category(*)
          ''')
          .eq('created_by', user.id)
          // .order('timestamp', ascending: true)
          .execute();
      if (res.hasError) {
        final errorMessage = res.error.toString();
        throw Exception(errorMessage);
      } else if (res.data != null) {
        return _parseData(res.data);
      }
      return List.empty();
    } else {
      throw Exception('No user');
    }
  }

  Future<TimeModel> createTime(CreateTimeModel createTime) async {
    final user = await getUser();
    if (user != null) {
      final res = await client.from('times').insert([
        {
          'name': createTime.name,
          'created_by': user.id,
          'timestamp': createTime.timestamp.toUtc().toIso8601String(),
          'image_url': createTime.imageUrl,
          'description': createTime.description,
          'frequency': createTime.frequency,
        }
      ]).execute();
      if (res.hasError) {
        final errorMessage = res.error.toString();
        throw Exception(errorMessage);
      } else if (res.data != null) {
        return TimeModel.fromJson(res.data[0]);
      }
    }
    throw Exception('No user');
  }

  Future<Uint8List> getImage(String imagePath) async {
    final user = await getUser();
    if (user != null) {
      final res = await client.storage.from('images').download(imagePath);
      if (res.hasError) {
        final errorMessage = res.error.toString();
        throw Exception(errorMessage);
      } else if (res.data != null) {
        return res.data!;
      } else {
        throw Exception('No image found');
      }
    } else {
      throw Exception('No user');
    }
  }

  Future<void> removeImageUrlAndDeleteImage(
      int timesId, String imagePath) async {
    try {
      await deleteImage(imagePath);
      final update = {'image_url': null};
      await updateTime(timesId, update);
    } catch (err) {
      rethrow;
    }
  }

  Future<TimeModel> updateTime(int id, Map<String, dynamic> update) async {
    final user = await getUser();
    if (user != null) {
      final res =
          await client.from('times').update(update).match({'id': id}).execute();
      if (res.hasError) {
        final errorMessage = res.error.toString();
        throw Exception(errorMessage);
      } else if (res.data != null) {
        return _parseData(res.data)[0];
      } else {
        throw Exception('No data returned');
      }
    } else {
      throw Exception('No user');
    }
  }

  Future<void> deleteTime(TimeModel time) async {
    final user = await getUser();
    if (user != null) {
      if (time.imageUrl != null) {
        deleteImage(time.imageUrl!);
      }
      final res =
          await client.from('times').delete().match({'id': time.id}).execute();
      if (res.hasError) {
        final errorMessage = res.error.toString();
        throw Exception(errorMessage);
      }
    } else {
      throw Exception('No user');
    }
  }

  Future<void> deleteImage(String imagePath) async {
    final delete = await client.storage.from('images').remove([imagePath]);
    if (delete.hasError) {
      throw Exception('Failed to delete image');
    }
  }

  Future<void> uploadImage(ImageUpdateModel imageUpdate) async {
    final res = await client.storage
        .from('images')
        .uploadBinary(imageUpdate.imageUrl, imageUpdate.image);
    if (res.hasError) {
      final errorMessage = res.error.toString();
      throw Exception(errorMessage);
    }
  }

  List<TimeModel> _parseData<T>(List<dynamic> data) {
    return data.map((e) => TimeModel.fromJson(e)).toList();
  }
}
