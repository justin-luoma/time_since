import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:supabase_flutter/supabase_flutter.dart' as sb;
import 'package:time_since/components/auth/auth_required_state.dart';
import 'package:time_since/times/components/image_display.dart';
import 'package:time_since/times/models/image_update_model.dart';
import 'package:time_since/times/models/time_model.dart';
import 'package:time_since/times/view_models/times_view_model.dart';
import 'package:time_since/utils/constants.dart';

class UpdateImagePage extends StatefulWidget {
  const UpdateImagePage({Key? key}) : super(key: key);

  @override
  _UpdateImagePageState createState() => _UpdateImagePageState();
}

class _UpdateImagePageState extends AuthRequiredState<UpdateImagePage> {
  bool _loading = false;

  sb.User? _user;
  TimeModel? _time;

  @override
  void dispose() {
    // if (!_finishedCreate && _imageFilePath != null) {
    //   supabase.storage
    //       .from('images')
    //       .remove([_imageFilePath!]).catchError((error) => debugPrint(error));
    // }
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    Future.microtask(() {
      final timesViewModel =
          Provider.of<TimesViewModel>(context, listen: false);
      _user = timesViewModel.currentUser;
      _time = timesViewModel.timeBeingEdited;
    });
  }

  @override
  Widget build(BuildContext context) {
    final vm = Provider.of<TimesViewModel>(context);

    final TimeModel? time = vm.timeBeingEdited;

    final Uint8List? image = vm.imageBeingUpdated != null
        ? vm.imageBeingUpdated?.image
        : time?.image;

    void _removeImage() async {
      final id = time?.id;
      final imageUrl = time?.imageUrl;
      if (id != null && imageUrl != null) {
        setState(() {
          _loading = true;
        });
        final success = await vm.removeImage(id, imageUrl);
        setState(() {
          _loading = false;
        });
        if (success && mounted) {
          vm.timeBeingEdited?.image = null;
          vm.setImageBeingUpdated(null);
          Navigator.of(context).pop();
        } else if (!success) {
          context.showErrorSnackBar(
              message: 'Failed to remove image, please '
                  'try again later');
        }
      }
    }

    // Future<void> _onCompletedUpdateImage(
    //     String filePath, Uint8List imageBytes) async {
    //   final update = ImageUpdateModel(imageBytes, filePath);
    //   vm.imageBeingUpdated = update;
    //   setState(() {
    //     if (_time != null) {
    //       _time!.image = imageBytes;
    //     }
    //     _imageFilePath = filePath;
    //   });
    // }

    void _onNewImageSelected(String fileName, Uint8List imageBytes) {
      final filePath = '${_user!.id}/$fileName';
      final imageUpdate = ImageUpdateModel(imageBytes, filePath);
      vm.setImageBeingUpdated(imageUpdate);
    }

    return Scaffold(
      appBar: AppBar(
        title: time != null
            ? Text('Update Image for ${time.name}')
            : const Text('New Image'),
        actions: <Widget>[
          IconButton(
            icon:
                _loading ? const Icon(Icons.pending) : const Icon(Icons.check),
            onPressed: (_time?.image != null || image != null) && !_loading
                ? () async {
                    Navigator.of(context).pop(true);
                  }
                : null,
          ),
        ],
      ),
      body: ListView(
        padding: const EdgeInsets.symmetric(vertical: 18, horizontal: 12),
        children: [
          ImageDisplay(
            imageBytes: image,
            newImageSelected: _onNewImageSelected,
          ),
          const SizedBox(
            height: 18,
          ),
          if (time != null)
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ElevatedButton(
                  onPressed: _loading
                      ? null
                      : time.image != null
                          ? _removeImage
                          : null,
                  style: ElevatedButton.styleFrom(
                    primary: Colors.pink,
                  ),
                  child: const Text('Remove'),
                )
              ],
            )
        ],
      ),
    );
  }

// Future<void> _saveImage() async {
//   final res = await supabase.from('times').update(
//       {'image_url': _imageFilePath}).match({'id': widget.time.id}).execute();
//   if (res.hasError) {
//     debugPrint(jsonEncode(res.error));
//   } else {
//     setState(() {
//       _finishedCreate = true;
//     });
//     widget.callback(_image);
//     Navigator.of(context).pop();
//   }
// }

}
