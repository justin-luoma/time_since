import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:provider/provider.dart';
import 'package:time_since/category/components/new_category_dialog.dart';
import 'package:time_since/category/models/category_dropdown_item.dart';
import 'package:time_since/category/models/category_model.dart';
import 'package:time_since/category/view_models/category_view_model.dart';
import 'package:time_since/colors/view_models/color_picker_view_model.dart';
import 'package:time_since/components/auth/auth_required_state.dart';
import 'package:time_since/times/pages/update_image_page.dart';
import 'package:time_since/times/models/create_time_model.dart';
import 'package:time_since/times/models/image_update_model.dart';
import 'package:time_since/times/models/time_model.dart';
import 'package:time_since/times/view_models/times_view_model.dart';
import 'package:time_since/utils/constants.dart';
import 'package:time_since/utils/validators.dart';

class TimeUpdatePage extends StatefulWidget {
  const TimeUpdatePage({Key? key}) : super(key: key);

  @override
  _TimeUpdatePageState createState() => _TimeUpdatePageState();
}

class _TimeUpdatePageState extends AuthRequiredState<TimeUpdatePage> {
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  static final noneCategory = {CategoryDropdownItem(0, 'None')};

  late final TextEditingController nameController;
  late final TextEditingController descController;
  late final TextEditingController frequencyController;
  late final TextEditingController dateTimeController;

  late DateTime _currentWhen;

  Set<CategoryDropdownItem> _categories = noneCategory;

  bool _whenUpdated = false;
  bool _loading = false;

  int? _selectedCategory;

  @override
  void initState() {
    super.initState();
    nameController = TextEditingController();
    descController = TextEditingController();
    frequencyController = TextEditingController();
    dateTimeController = TextEditingController();
    _categories = noneCategory;
    Future.microtask(() async {
      final vm = Provider.of<TimesViewModel>(context, listen: false);
      final categoryViewModel =
          Provider.of<CategoryViewModel>(context, listen: false);
      await categoryViewModel.getCategories();
      final categories = categoryViewModel.categories;
      _mapCategoryDropdowns(categories);
      final time = vm.timeBeingEdited;
      if (time != null) {
        nameController.text = time.name;
        frequencyController.text =
            time.frequency != null ? time.frequency.toString() : '';
        _currentWhen = time.timestamp.toLocal();
        dateTimeController.text = _buildWhenLine(time.timestamp.toLocal());
        _selectedCategory = time.category != null ? time.category!.id : 0;
      } else {
        final now = DateTime.now();
        _currentWhen = now;
        dateTimeController.text = _buildWhenLine(now);
      }
    });
  }

  void _mapCategoryDropdowns(List<CategoryModel> categories) {
    if (_categories.length != categories.length + 1) {
      setState(() {
        _categories
            .addAll(categories.map((e) => CategoryDropdownItem(e.id, e.name)));
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final timesViewModel = Provider.of<TimesViewModel>(context);

    final time = timesViewModel.timeBeingEdited;

    final imageBeingUpdated = timesViewModel.imageBeingUpdated;

    final defaultColor =
        Theme.of(context).cardTheme.color ?? Theme.of(context).cardColor;

    Uint8List? image =
        imageBeingUpdated != null ? imageBeingUpdated.image : time?.image;

    Future<void> save() async {
      if (formKey.currentState!.validate()) {
        final name = nameController.text;
        final desc = descController.text;
        final frequency = frequencyController.text.isNotEmpty
            ? int.parse(frequencyController.text)
            : null;
        final when = _currentWhen;
        final category = _selectedCategory == 0 ? null : _selectedCategory;
        if (time != null) {
          await saveTimeUpdate(name, time, frequency, desc, when, category,
              imageBeingUpdated, timesViewModel, context);
        } else {
          final CreateTimeModel createTime = CreateTimeModel(
              name, when, desc, imageBeingUpdated?.imageUrl, frequency,
              image: imageBeingUpdated?.image);
          try {
            setState(() {
              _loading = true;
            });
            await timesViewModel.createTime(createTime);
          } catch (err) {
            context.showErrorSnackBar(
                message: 'Failed to save, try again later.');
          }
          setState(() {
            _loading = false;
          });
        }
      }
    }

    Future<void> _refreshCategories(int created) async {
      setState(() {
        _categories = noneCategory;
      });
      final categoryViewModel =
          Provider.of<CategoryViewModel>(context, listen: false);
      await categoryViewModel.getCategories();
      final categories = categoryViewModel.categories;
      _mapCategoryDropdowns(categories);
      setState(() {
        _selectedCategory = created;
      });
    }

    return Scaffold(
      appBar: AppBar(
        title: time != null
            ? Text('Update ${time.name}')
            : const Text('Create new'
                ' item'),
        actions: <Widget>[
          IconButton(
              icon: _loading
                  ? const Icon(Icons.pending)
                  : const Icon(Icons.check),
              onPressed: _loading
                  ? null
                  : () async {
                      await save();
                      if (mounted) {
                        timesViewModel.imageBeingUpdated = null;
                        Navigator.of(context).pop(true);
                      }
                    }),
        ],
      ),
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: ListView(
          children: [
            Form(
              key: formKey,
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 8, horizontal: 12),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    TextFormField(
                      controller: nameController,
                      validator: Validators.requiredValidator,
                      textCapitalization: TextCapitalization.words,
                      decoration: const InputDecoration(labelText: 'Name'),
                    ),
                    TextFormField(
                      decoration:
                          const InputDecoration(labelText: 'Description'),
                      obscureText: false,
                      controller: descController,
                      textCapitalization: TextCapitalization.sentences,
                      minLines: 3,
                      maxLines: 5,
                    ),
                    TextFormField(
                      controller: frequencyController,
                      decoration: const InputDecoration(
                          labelText: 'Frequency '
                              'in days'),
                      inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                      keyboardType: TextInputType.number,
                    ),
                    TextField(
                      controller: dateTimeController,
                      decoration:
                          const InputDecoration(labelText: 'Date and Time'),
                      readOnly: true,
                      onTap: () => _selectWhen(context),
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Flexible(
                          fit: FlexFit.tight,
                          child: DropdownButtonFormField<int>(
                            value: _selectedCategory ??
                                (time?.category != null &&
                                        _categories.length > 1
                                    ? time?.category?.id
                                    : 0),
                            decoration:
                                const InputDecoration(labelText: 'Category'),
                            onChanged: (value) {
                              setState(() {
                                _selectedCategory = value;
                              });
                            },
                            items: _categories
                                .map((e) => DropdownMenuItem<int>(
                                      value: e.id,
                                      child: Text(e.name),
                                    ))
                                .toList(),
                          ),
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            TextButton.icon(
                              onPressed: () async {
                                final int? created = await showDialog(
                                    context: context,
                                    builder: (ctx) => MultiProvider(
                                          providers: [
                                            ChangeNotifierProvider(
                                              create: (context) =>
                                                  ColorPickerViewModel(
                                                      defaultColor),
                                            ),
                                            ChangeNotifierProvider.value(
                                                value: Provider.of<
                                                    CategoryViewModel>(context))
                                          ],
                                          child: const NewCategoryDialog(),
                                        ));
                                if (created != null && mounted) {
                                  await _refreshCategories(created);
                                }
                              },
                              label: const Text(
                                'Add Category',
                              ),
                              icon: const Icon(Icons.add),
                            ),
                          ],
                        )
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 18.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(bottom: 8.0),
                            child: Text(
                              'Image',
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                  color: Theme.of(context)
                                      .textTheme
                                      .headline1
                                      ?.color),
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              GestureDetector(
                                onTap: () async {
                                  final bool? finished =
                                      await Navigator.of(context).push(
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  const UpdateImagePage()));
                                  if (finished != null) {
                                    debugPrint('#### new image ####');
                                  } else {
                                    timesViewModel.setImageBeingUpdated(null);
                                    debugPrint('#### no image ####');
                                  }
                                },
                                child: image != null
                                    ? Image.memory(
                                        image,
                                        height: 300,
                                        width: 300,
                                        fit: BoxFit.contain,
                                        alignment: Alignment.center,
                                      )
                                    : SizedBox(
                                        width: 300,
                                        height: 300,
                                        child: Container(
                                          color: Colors.grey,
                                          child: const Center(
                                            child: Text('Add image'),
                                          ),
                                        ),
                                      ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> saveTimeUpdate(
      String name,
      TimeModel time,
      int? frequency,
      String desc,
      DateTime when,
      int? category,
      ImageUpdateModel? imageBeingUpdated,
      TimesViewModel timesViewModel,
      BuildContext context) async {
    Map<String, dynamic>? update;
    if (name != time.name ||
        frequency != time.frequency ||
        desc != time.description ||
        time.category?.id != category ||
        _whenUpdated) {
      update = {
        'name': name,
        'description': desc,
        'frequency': frequency,
        'category': category,
        if (_whenUpdated) 'timestamp': when.toUtc().toIso8601String(),
      };
    }

    try {
      if (update != null && imageBeingUpdated == null) {
        setState(() {
          _loading = true;
        });
        await timesViewModel.updateTime(time.id, update);
      } else if (update != null && imageBeingUpdated != null) {
        setState(() {
          _loading = true;
        });
        await timesViewModel.updateTime(time.id, update,
            imageUpdate: imageBeingUpdated);
      } else if (imageBeingUpdated != null) {
        setState(() {
          _loading = true;
        });
        await timesViewModel.updateImage(time.id, imageBeingUpdated);
      }
    } catch (err) {
      context.showErrorSnackBar(message: 'Failed to save, try again later.');
    }
    setState(() {
      _loading = false;
    });
  }

  void _selectWhen(BuildContext context) {
    DatePicker.showDateTimePicker(context,
        currentTime: _currentWhen,
        onConfirm: (date) => {
              setState(() {
                _whenUpdated = true;
                _currentWhen = date;
                dateTimeController.text = _buildWhenLine(date);
              })
            });
  }

  String _buildWhenLine(DateTime currentWhen) {
    final now = DateTime.now();

    final diff = now.difference(currentWhen).inDays;

    if (diff == 0) {
      return 'Today ${timeFormat.format(currentWhen.toLocal())}';
    }

    return dateTimeFormat.format(currentWhen.toLocal());
  }
}
