import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:time_since/category/category_screen.dart';
import 'package:time_since/components/auth/auth_required_state.dart';
import 'package:time_since/models/loading_status.dart';
import 'package:time_since/times/components/time_card.dart';
import 'package:time_since/times/models/time_model.dart';
import 'package:time_since/times/view_models/times_view_model.dart';
import 'package:time_since/utils/time.dart';

class ImagePath {
  final int id;
  final String path;

  ImagePath(this.id, this.path);
}

class TimesScreen extends StatefulWidget {
  const TimesScreen(
    this.context, {
    Key? key,
  }) : super(key: key);

  final BuildContext context;

  @override
  _TimesScreenState createState() => _TimesScreenState();
}

class _TimesScreenState extends AuthRequiredState<TimesScreen> {
  final Map<int, bool> _folderOpen = Map.identity();

  @override
  void initState() {
    super.initState();
    Future.microtask(() =>
        Provider.of<TimesViewModel>(context, listen: false).refreshTimes());
  }

  @override
  Widget build(BuildContext context) {
    final timesViewModel = Provider.of<TimesViewModel>(context);
    final tabController = Provider.of<TabController>(context, listen: false);

    final List<TimeModel> times = timesViewModel.times.values.toList();
    times.sort(timestampSorter);
    return TabBarView(
      controller: tabController,
      children: [
        CategoryScreen(),
        RefreshIndicator(
          onRefresh: () async {
            await timesViewModel.getTimes();
          },
          child: timesViewModel.loadingStatus.status == Status.loading
              ? const Center(
                  child: CircularProgressIndicator(),
                )
              : timesViewModel.loadingStatus.status == Status.error
                  ? ListView(
                      children: [
                        Center(
                          child: Text(timesViewModel.loadingStatus.message!),
                        ),
                      ],
                    )
                  : timesViewModel.loadingStatus.status == Status.initial
                      ? Text(timesViewModel.loadingStatus.message!)
                      : _mainContent(times),
        ),
      ],
    );
  }

  Widget _mainContent(List<TimeModel> times) {
    return times.isNotEmpty
        ? ListView.builder(
            itemCount: times.length,
            itemBuilder: (context, index) =>
                _buildCard(times.elementAt(index), context),
            padding: const EdgeInsets.symmetric(vertical: 18, horizontal: 12),
          )
        : ListView(
          children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: const [
                  Padding(
                    padding: EdgeInsets.only(top: 18.0),
                    child: Text('No items'),
                  )
                ],
              ),
            ],
        );
  }

  Widget _buildCard(TimeModel time, BuildContext context) {
    final cardOpen = _folderOpen.containsKey(time.id) && _folderOpen[time.id]!;
    return TimeCard(time, cardOpen, (bool open) {
      setState(() {
        _folderOpen.update(
          time.id,
          (value) => open,
          ifAbsent: () => open,
        );
      });
    });
  }
}
