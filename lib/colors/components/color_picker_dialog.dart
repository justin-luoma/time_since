import 'package:flutter/material.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:provider/provider.dart';
import 'package:time_since/colors/view_models/color_picker_view_model.dart';

class ColorPickerDialog extends StatelessWidget {
  const ColorPickerDialog({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final vm = Provider.of<ColorPickerViewModel>(context);

    return AlertDialog(
      content: SingleChildScrollView(
        child: ColorPicker(
          onColorChanged: (Color color) => vm.selectedColor = color,
          pickerColor: vm.color,
          displayThumbColor: true,
        ),
      ),
      actions: [
        TextButton(
          onPressed: () {
            vm.updateColor();
            Navigator.of(context).pop();
          },
          child: const Text('Select'),
        ),
        TextButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          child: const Text('Cancel'),
        )
      ],
    );
  }
}
