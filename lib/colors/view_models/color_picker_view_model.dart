import 'package:flutter/material.dart';

class ColorPickerViewModel extends ChangeNotifier {
  ColorPickerViewModel(this.color);

  Color color;

  Color? selectedColor;

  void updateColor() {
    if (selectedColor != null) {
      color = selectedColor!;
      notifyListeners();
    }
  }
}
