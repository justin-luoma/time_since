import 'dart:async';
import 'dart:convert';
import 'dart:ui';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_background_service/flutter_background_service.dart';
import 'package:flutter_background_service_android/flutter_background_service_android.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:time_since/config.dart';
import 'package:time_since/utils/constants.dart';

class NotifierService {
  static NotifierService get instance {
    return _instance;
  }

  NotifierService._();

  static final NotifierService _instance = NotifierService._();

  final FlutterLocalNotificationsPlugin _notificationsPlugin =
      FlutterLocalNotificationsPlugin();
  var _initialized = false;
  SupabaseClient? _supabase;

  Future<void> init() async {
    if (!_initialized) {
      debugPrint('NotifierService init');
      await _initializeNotifications();
      await _initializeSupabase();
      _initialized = true;
    }
  }

  Future<void> startBackgroundService(ServiceInstance service) async {
    DartPluginRegistrant.ensureInitialized();
    if (_supabase == null) {
      await _initializeSupabase();
    }

    _setAndroidInfo(service);

    Timer.periodic(const Duration(hours: 5), (timer) async {
      await _initializeSupabase();
      final Session? session = _supabase?.auth.currentSession;
      if (session != null && session.expiresAt != null) {
        final expirationTime =
            DateTime.fromMillisecondsSinceEpoch(session.expiresAt! * 1000);
        final now = DateTime.now();
        if (now.isBefore(expirationTime)) {
          final diff = expirationTime.difference(now);
          debugPrint(diff.toString());
          if (diff.inDays < 1) {
            debugPrint('refresh');
            await _supabase?.auth.refreshSession();
          }
        }
      }
    });

    Duration duration = const Duration(hours: 1);

    Timer.periodic(duration, (timer) async {
      await _runNotificationProcessor();
    });
  }

  Future<SupabaseClient> _initializeSupabase() async {
    try {
      final supabase = await Supabase.initialize(
        url: Config.supabaseURL,
        anonKey: Config.supabaseAnonPublicKey,
      );
      _supabase = supabase.client;
      return supabase.client;
    } catch (err) {
      return supabase;
    }
  }

  Future<void> _runNotificationProcessor() async {
    if (_isTimeWindow() && await _shouldSendNotification()) {
      debugPrint('notification processing');
      final user = _supabase!.auth.user();
      if (user != null) {
        PostgrestResponse<dynamic> response = await _fetchTimes(user.id);
        final data = response.data;
        if (data != null) {
          final timesDueToday = _processTimers(data);
          if (timesDueToday.isNotEmpty) {
            await _showTimesNotification(timesDueToday);
          }
        }
      }
    }
  }

  Future<void> _showTimesNotification(List<dynamic> timesDueToday) async {
    final format = DateFormat('HH:mm');
    final List<String> lines = List.empty(growable: true);
    var id = 0;
    for (final time in timesDueToday) {
      final timestamp = DateTime.parse(time['timestamp']).toLocal();
      lines.add('${time['name']} due at ${format.format(timestamp)}');
      id += time['id'] as int;
      // await _notificationsPlugin.show(time!['id'] as int, 'Item(s) due today',
      //     '${time['name']} due at ${format.format(timestamp)}', details);
    }
    InboxStyleInformation styleInformation = InboxStyleInformation(lines);
    AndroidNotificationDetails androidDetails = AndroidNotificationDetails(
        Config.UPCOMING_TIME_CHANNEL_ID, Config.UPCOMING_TIME_CHANNEL_NAME,
        importance: Importance.max,
        priority: Priority.high,
        styleInformation: styleInformation,
        groupKey: 'dev.luoma.time_since.TIME_GROUP');
    NotificationDetails details = NotificationDetails(android: androidDetails);
    await _notificationsPlugin.show(
        id,
        "Item(s) due today",
        'You have '
            '${lines.length} due today',
        details);

    final prefs = await SharedPreferences.getInstance();

    await prefs.setString(
        Config.lastNotificationKey, DateTime.now().toIso8601String());
  }

  Future<PostgrestResponse<dynamic>> _fetchTimes(userId) async {
    final response = await _supabase!
        .from('times')
        .select()
        .eq('created_by', userId)
        .order('timestamp', ascending: true)
        .execute();
    final error = response.error;
    if (error != null && response.status != 406) {
      debugPrint(error.toString());
    }
    return response;
  }

  List _processTimers(List<dynamic> data) {
    List<dynamic> thingsDueToday = List.empty(growable: true);

    for (var time in data) {
      final frequency = time['frequency'];
      if (frequency != null) {
        final timestamp = DateTime.parse(time['timestamp']);

        final now = DateTime.now();
        final diff = now.difference(timestamp);

        final days = diff.inDays;
        final dayHours = days * 24;
        final hours = diff.inHours - dayHours;

        if (days >= frequency) {
          thingsDueToday.add(time);
        } else if (days == frequency - 1 && hours > now.hour) {
          thingsDueToday.add(time);
        }
      }
    }

    if (thingsDueToday.isNotEmpty) {
      if (kDebugMode) {
        print(jsonEncode(thingsDueToday));
      }
    }
    return thingsDueToday;
  }

  bool _isTimeWindow() {
    final now = DateTime.now();

    return now.hour >= 7 && now.hour <= 10;
  }

  Future<bool> _shouldSendNotification() async {
    final prefs = await SharedPreferences.getInstance();

    final lastNotification = prefs.getString(Config.lastNotificationKey);
    if (lastNotification != null) {
      final now = DateTime.now();
      final parsed = DateTime.parse(lastNotification);

      return now.day != parsed.day;
    }

    return true;
  }

  Future<void> _initializeNotifications() async {
    const AndroidInitializationSettings androidSettings =
        AndroidInitializationSettings('icon');

    const IOSInitializationSettings iosSettings = IOSInitializationSettings();

    const InitializationSettings settings = InitializationSettings(
      android: androidSettings,
      iOS: iosSettings,
    );

    await _notificationsPlugin.initialize(settings);
  }

  FutureOr<bool> onIosBackground(ServiceInstance service) async {
    WidgetsFlutterBinding.ensureInitialized();
    if (_supabase == null) {
      await _initializeSupabase();
    }

    if (kDebugMode) {
      print('FLUTTER BACKGROUND FETCH');
    }

    return true;
  }

  Future<void> _setAndroidInfo(ServiceInstance service) async {
    if (service is AndroidServiceInstance) {
      service.setForegroundNotificationInfo(
          title: 'Time Since Service', content: 'Running');
    }
  }
}
