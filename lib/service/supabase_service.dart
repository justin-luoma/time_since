import 'package:flutter/foundation.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

abstract class SupabaseService {
  final SupabaseClient client;

  SupabaseService(this.client);

  Future<User?> getUser() async {
    await _refreshToken();
    return client.auth.user();
  }

  Future<void> _refreshToken() async {
    final session = client.auth.currentSession;
    if (session != null && session.expiresAt != null) {
      final expirationTime =
          DateTime.fromMillisecondsSinceEpoch(session.expiresAt! * 1000);
      final now = DateTime.now();
      if (now.isBefore(expirationTime)) {
        final diff = expirationTime.difference(now);
        if (diff.inMinutes < 15) {
          debugPrint('refreshing token');
          await client.auth.refreshSession();
        }
      }
    }
  }
}
