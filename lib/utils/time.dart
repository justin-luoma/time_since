import 'package:time_since/category/models/categorized_times_model.dart';
import 'package:time_since/times/models/time_model.dart';
import 'package:time_since/utils/constants.dart';

String getDateAndFrequency(TimeModel time) {
  final frequency = time.frequency;
  if (frequency != null) {
    return '${formatDate(time.timestamp)} - $frequency day(s)';
  }
  return formatDate(time.timestamp);
}

String parseDate(String time) {
  final dateTime = DateTime.parse(time);

  return dateTimeFormat.format(dateTime.toLocal());
}

String formatDate(DateTime date) {
  return dateTimeFormat.format(date.toLocal());
}

bool isOverdue(TimeModel time) {
  final frequency = time.frequency;

  if (frequency != null) {
    final now = DateTime.now();
    final timestamp = time.timestamp;
    final diff = now.difference(timestamp);

    return diff.inHours / 24 > frequency;
  }

  return false;
}

bool isDue(TimeModel time) {
  final now = DateTime.now();
  if (time.frequency != null) {
    return now.isAfter(time.timestamp.add(Duration(days: time.frequency!)));
  }
  return false;
}

String timeSince(DateTime time) {
  final now = DateTime.now();
  final diff = now.difference(time);

  if (diff.inDays > 0) {
    final dayHours = diff.inDays * 24;
    final hours = diff.inHours - dayHours;

    return '${diff.inDays}d ${hours}h';
  }

  return '${diff.inHours}h';
}

int timestampSorter(a, b) => a.timestamp.compareTo(b.timestamp);

int dueDateSorter(a, b) => a.dueDate == null
    ? 1
    : b.dueDate == null
        ? -1
        : a.dueDate.compareTo(b.dueDate);

int categoryDueDateSorter(CategorizedTimesModel a, CategorizedTimesModel b)
=> a.times.isEmpty ? 1 : a.times.first.dueDate == null ? 1 : b.times.isEmpty
    ? -1 : b.times.first.dueDate == null ? -1 : a.times.first.dueDate!
    .compareTo(b.times.first.dueDate!);
